#![allow(unused_imports)]

use std::path::PathBuf;
use anyhow::Result;
use ew_control::{
    connection_layout,
    supersequence,
    prelude::{
        Computer,
        Connection,
        Event,
        SeqColor,
        Sequence,
        SequenceViewer,
        SuperSequence,
    },
};

fn main() -> Result<()> {
    let c = connection_layout!(
        "scope_trig"        => Connection::Digital(3, 2, false, 0.0, 0.0),
        "andor_trig"        => Connection::Digital(3, 4, false, 27e-3, 27e-3),
        "tweezer_aod_am"    => Connection::Analog(9, 4.465, 0.0),
        "qinit_green_aom"   => Connection::Digital(1, 19, true, 0.0, 0.0),
        "qinit_green_aom_0" => Connection::Digital(1, 2, false, 0.0, 0.0),
        "qinit_green_sh"    => Connection::Digital(1, 9, false, 1.5e-3, 1.5e-3),
        "mot3_coils_sig"    => Connection::Digital(0, 8, false, 0.0, 0.0),
        "mot3_coils_clk"    => Connection::Digital(0, 10, false, 0.0, 0.0),
        "mot3_coils_sync"   => Connection::Digital(0, 14, true, 0.0, 0.0),
    );

    let sseq = supersequence!(
        sequences: {
            "scope" => Sequence::new([
                Event::pulse(c["scope_trig"], 1, 0.1..0.2)?,
            ]).with_color(Some(SeqColor::C0)),

            "andor" => Sequence::new([
                Event::pulse(c["andor_trig"], 1, 0.1..0.2)?,
            ]).with_color(Some(SeqColor::C2)),

            "tweezer ramp" => Sequence::new([
                Event::analog_ramp(c["tweezer_aod_am"], 4.465..0.000, 0.10..0.15, 100)?,
                Event::analog_ramp(c["tweezer_aod_am"], 0.000..4.465, 0.15..0.20, 100)?,
            ]).with_color(Some(SeqColor::C8)),

            "coil ramp" => Sequence::new([
                Event::serial_ramp(
                    (
                        c["mot3_coils_sig"],
                        Some(c["mot3_coils_clk"]),
                        // None,
                        Some(c["mot3_coils_sync"]),
                        // None,
                    ),
                    (441815_u32, 20)..(441815_u32 / 10, 20),
                    (1_u32, 4),
                    0.10..0.15,
                    100,
                )?,
                Event::serial_ramp(
                    (
                        c["mot3_coils_sig"],
                        Some(c["mot3_coils_clk"]),
                        Some(c["mot3_coils_sync"]),
                    ),
                    (441815_u32 / 10, 20)..(441815_u32, 20),
                    (1_u32, 4),
                    0.15..0.20,
                    100,
                )?,
            ]).with_color(Some(SeqColor::C1)),
        },
        parameters: { }
    );

    sseq.save(PathBuf::from("./save-sequence"), true)?;
    let sseq_load
        = SuperSequence::load(PathBuf::from("./save-sequence"), true)?;
    assert_eq!(sseq, sseq_load);
    SequenceViewer::run(&sseq, &c, None)?;

    // let mut computer = Computer::connect(([128, 174, 248, 206], 50100), Some(1), Some(1.0), c.clone())?;
    // println!("connected to computer");

    // computer.enqueue(sseq.to_states()?)?;
    // println!("enqueued sequence");

    // computer.run(true)?;
    // println!("ran sequence successfully");

    // computer.clear()?;
    // println!("cleared sequence memory");

    // computer.set_defaults()?;
    // println!("set defaults");

    // computer.disconnect();
    // println!("disconnected from computer");

    Ok(())
}
