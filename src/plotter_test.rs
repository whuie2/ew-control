use ew_control::{
    connection_layout,
    supersequence,
    structures::{
        Connection,
        Event,
        Sequence,
    },
    viewer::SequenceViewer,
};

fn main() {
    let connections = connection_layout!(
        "connection 1" => Connection::Digital(0, 0, false, 0.0, 5e-6),
        "connection 2" => Connection::Digital(0, 1, false, 0.0, 0.0),
        "connection 3" => Connection::Digital(0, 2, false, 0.0, 0.0),
        "connection 4" => Connection::Analog(0, 0.0, 1e-3),
    );

    let sseq = supersequence!(
        sequences: {
            "sequence A" => Sequence::new([
                Event::serial_ramp(
                    (
                        connections["connection 1"],
                        Some(connections["connection 2"]),
                        Some(connections["connection 3"]),
                    ),
                    (0_u32, 3)..(5_u32, 3),
                    (2_u32, 2),
                    0.5..1.0,
                    5,
                ).unwrap(),
            ]),
            "sequence B" => Sequence::new([
                Event::pulse(
                    connections["connection 1"],
                    1,
                    1.5..2.0,
                ).unwrap(),
            ]),
            "sequence C" => Sequence::new([
                Event::analog_ramp(
                    connections["connection 4"],
                    -10.0..10.0,
                    0.5..1.0,
                    100,
                ).unwrap(),
            ]),
            "sequence D" => Sequence::new([
                Event::analog_ramp(
                    connections["connection 4"],
                    10.0..-10.0,
                    1.0..1.5,
                    100,
                ).unwrap(),
            ]),
        },
        parameters: { }
    );
    SequenceViewer::run(&sseq, &connections, None).unwrap();
}

