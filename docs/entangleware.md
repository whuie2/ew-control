# Entangleware interface

## Backend
Experiment control is based around communication with the two National
Instruments cards installed in the computer, the [PCIe-7820][pcie-7820], and the
[PCIe-6738][pcie-6738]. The former holds the digital I/O channels, of which
there are a total of 128 spread across four connectors (32 channels each), and
the latter holds the analog output channels, of which there are 32 on a single
connector. The backend to talk to these boards, provided by Joshua Zirbel,
comprises a low-level LabVIEW layer for direct communication with the cards and
a high-level Python module for interfacing with the LabVIEW layer.

The Python module is additionally built to handle communication with the cards
over a local network via the computer they're installed on. The default (and
currently only) behavior is to send messages to the computer housing them over
the network, provided the correct address on the network. While this means any
computer is able to issue commands to the cards, this function also forces a bit
of redundancy when commands are being issued by the computer in which the cards
are stored. Currently there are plans to remove this redundancy, but changes
have not yet been implemented.

## Frontend
The frontend I have written takes an object-oriented approach to building on the
elements of the backend to hopefully make the programs controlling these boards
more readable and to facilitate other functionalities like visualization. The
frontend introduces three main object types: `Event`, `Sequence`, and
`Computer`. More exact definitions for behavior can be found in the source
files, but this section will give an overview of these classes for minimal
usage.

### `Event`
`Event` is a `dict`-like class that holds the information required for some
command to be issued to either of the cards at a certain time. The `Event` class
manages settings for both analog and digital output, which are distinguished
from each other through the use of the accompanying enum `EventType` (whose
variants are `.Analog` and `.Digital`). The constructor has the signature:

```Python
class Event:
    def __init__(self, kind: EventType, time: float=None, **data):
        ...
```

The `**data` keyword parameters required for a valid `Event` instance depend on
the value of the `EventType` passed to the constructor. If `EventType.Analog` is
passed, the constructor will require values for the parameters `channel` (`int`)
and `state` (`float`). These parameters represent the channel number (0-31), and
the floating-point voltage to output. `state` values are automatically rounded
to the nearest voltage achievable by the card, given 16-bit resolution over the
+/- 10 V range. `board` may also be passed, but since there is currently only
one board this parameter may be omitted, in which case `board=0` is assumed.

If `Event.Digital` is passed, the constructor will require values for the
parameters `connector` (`int`), `mask` (`int`), and `state` (`int`). As with the
analog side, we currently only have one digital output board (so the same
behavior applies), but unlike the PCI-6738, the PCIe-7820 has four connectors
(numbered 0-3). The parameters `mask` and `state` are integers representing bit
arrays that describe the activation of the channels on a given connector (32
channels on each), where a `1` in the $k$-th position indicates HIGH on the
$k$-th channel (and a `0` indicates LOW). While `state` describes the channels'
HIGH/LOW states, `mask` provides additional control by specifying which of these
channels' states are actually driven by the board. The actual resulting state of
all the channels on the connector can be thought of as the bitwise AND of `mask`
and `state`. In the following example, `mask` has `1`'s in places 0, 3, 17, 21,
and 28, while `state` has `1`'s in places 0, 4, 17, 21, and 27, so the actual
output of the card will be HIGH on channels 0, 17, and 21, and LOW everywhere
else:
```
  mask = 00010000001000100000000000001001 = 270663689
 state = 00001000001000100000000000010001 = 136445969
result = 00000000001000100000000000000001 = 2228225
```
To assist with the construction of these bit arrays, the frontend provides the
useful function `ch()`, which handles the placement of `1`'s in their
appropriate spots. To produce the value of `state` shown above, the
corresponding function call would be
```Python
from lib import ch

state = ch(0, 4, 17, 21, 27)
```

Every `Event` is also required to have a time before it can be passed to the
backend and issued as a command to the card. This can be done by two methods.
The first is the standard Pythonic way, where the time (in seconds) is passed as
an additional keyword argument on construction:
```Python
from lib import Event, EventType, ch

my_event = Event(
    EventType.Digital,
    board=0, connector=0, mask=ch(0, 3, 17, 21, 28), state=ch(0, 4, 17, 21, 27),
    time=0.5
)
```
The second is by using the overloaded `__matmul__()` magic method (via the `@`
operator):
```Python
my_event = Event(
    EventType.Analog,
    board=0, channel=16, state=5.0
) @ 0.5
```
Both methods are equivalent.

### `Sequence`
`Sequence` is a `list`-like class that holds `Event`s. While `Sequence`s can, in
principle, be constructed from a pre-made `list` of `Event`s through its default
constructor, the preferred method (for the sake of readability) to populate a
`Sequence` is to first initialize an empty one, and then append or insert events
through the `.append()` or `.insert()` methods, or the overloaded `__lshift__()`
magic method (the `<<` operator):
```Python
from lib import Sequence, Event, EventType, ch

event1 = Event(
    EventType.Digital,
    board=0, connector=0, mask=ch(0, 3, 17, 21, 28), state=ch(0, 4, 17, 21, 27)
) @ 0.1

event2 = Event(
    EventType.Digital,
    board=0, connector=0, mask=ch(0, 3, 17, 21, 28), state=0
) @ 0.2

event2 = Event(
    EventType.Analog,
    board=0, channel=16, state=5.0
) @ 0.5

# via constructor
sequence1 = Sequence([event1, event2, event3])

# via .append(), .insert()
sequence2 = Sequence()
sequence2.append(event1)
sequence2.append(event3)
sequence2.insert(1, event2)

# via <<
sequence3 = (Sequence()
    << event1
    << event2
    << event3
)
```
`Sequence`s can also be constructed from lists of times and states (for either
digital or analog outputs) in the case of e.g. ramped outputs. Two `Sequence`s
may also be concatenated with the `+` operator.

### `Computer`
The `Computer` class handles enqueueing, building, and running sequences via the
backend. Only one `Computer` object should be instantiated and connected to at a
time. The basic sequence for how one interacts with a `Computer` should look
like this:

1. Instantiate a `Computer` object with IP address, port number, etc.
2. Tell the backend to connect to the provided address
3. Set any default channel states
4. Enqueue the desired sequence
5. Run the sequence
6. Clear the sequence from the boards' memory
7. Disconnect from the computer

where each item is an essentially distinct method in the `Computer` class. The
following gives a toy example showing how the `Computer` class may be used:
```Python
from lib import Sequence, Computer, EventType

my_sequence1 = Sequence()
# populate the sequence here #
my_sequence2 = Sequence()
# populate the sequence here #

C = Computer(address="128.174.248.206", port=50100).connect()
(C
    .zero(EventType.Digital) # zero out all digital channels
    .default(EventType.Analog, ch=0, state=1.0)  # set the default voltages
    .default(EventType.Analog, ch=1, state=10.0) # for a couple analog channels
    .enqueue(my_sequence1) # send the sequences to the board(s)
    .enqueue(my_sequence2)
    .run() # execute all sequences
    .clear() # clear the board memory
    .disconnect()
)
```
Note that the final two steps -- clearing and disconnecting -- must be included
in every program.

## Timeline visualization
> **Note:** Development is still ongoing in this part of the library; things may
> change such that the information presented here becomes inaccurate in the
> future.

The frontend also defines an additional class, `SuperSequence`. This is a
`dict`-like class meant to handle the organization and visualization of a
sequence of `Sequence`s, where each constituent is assigned a label so that
users can keep track of a high-level overview of a complicated series of events.
Unlike the other classes discussed above, the use of this class is optional and
only for the convenience of the user.

The main function of a `SuperSequence` is to programmatically assemble any set
of labeled `Sequence`s into an easily digestible, visual form via a `matplotlib`
backend. Currently only a simplified visualization has been implemented, but
detailed view is planned for the future.

## Hardware considerations
- The analog isolation box introduces a small shift in the final output voltage
  that must be accounted for by the user. The size of the shift varies with the
  input voltage from the board; some measurement must be done to fully
  characterize it. Once measured, we plan to compensate for it in the backend.

[pcie-7820]: https://www.ni.com/en-us/support/model.pcie-7820.html
[pcie-6738]: https://www.ni.com/en-us/shop/hardware/products/analog-output-device.html?skuId=185503

