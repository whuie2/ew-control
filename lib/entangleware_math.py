from __future__ import annotations
import math

def exponential_ramp(val_start: float, val_end: float, t_start: float,
        t_end: float, decay_rate: float) -> (list[float], list[float]):
    """
    This will generate two lists containing time data and analog data. This data
    will be an exponential decay function that will start at
    (t_start, val_start) and end at (t_end, val_end)

    Parameters
    ----------
    val_start : float
        Starting voltage of the ramp
    val_end : float
        Ending voltage of the ramp
    t_start : float
        Starting time of the ramp
    t_end : float
        Ending time of the ramp
    decay_rate : float
        Decay rate of the ramping exponential function

    Returns
    -------
    timesteps : list[float]
        List of times
    analogsteps : list[float]
        List of analog values
    """
    # Set start and end values to an integer; The intent is to format as a
    # 16-bit integer for the DAC. You may wish to warn the user if their initial
    # or final value is outside the range of the DAC
    q_val_start = int((val_start / 20) * (2 ** 16))
    q_val_end = int((val_end / 20) * (2 ** 16))
    if q_val_end == q_val_start:
        return [t_start, t_end], [val_start, val_end]
    delta = math.exp(decay_rate * (t_end - t_start))
    if delta == 1:
        raise ValueError('decay_rate or the time interval is too small')
    alpha = (q_val_start - q_val_end) / (1 - delta)
    offset = q_val_start - alpha
    if q_val_start > q_val_end:
        outputsteps = list(range(q_val_end, q_val_start + 1))
    else:
        outputsteps = list(range(q_val_start, q_val_end + 1))
    timesteps = [None] * len(outputsteps)
    analogsteps = [None] * len(outputsteps)
    for indx in range(len(outputsteps)):
        timesteps[indx] = t_start \
                + math.log((outputsteps[indx] - offset) / alpha) \
                / decay_rate
        analogsteps[indx] = 20 * outputsteps[indx] / (2**16)
    if q_val_start > q_val_end:
        timesteps.reverse()
        analogsteps.reverse()
    return (timesteps, analogsteps)

def linear_ramp(V0: float, V1: float, t0: float, t1: float) \
        -> (list[float], list[float]):
    """
    Generate times and voltages for a linear ramp from (t0, V0) to (t1, V1) at
    maximum voltage resolution.

    Parameters
    ----------
    V0, V1 : float
        Starting and ending voltages.
    t0, t1 : float
        Starting and ending times.

    Returns
    -------
    T, V : list[float]
    """
    q0 = int((V0 / 20) * (2 ** 16))
    q1 = int((V1 / 20) * (2 ** 16))
    if q1 == q0:
        return [t0, t1], [V0, V1]
    numsteps = abs(q0 - q1)
    dq = +1 if q1 > q0 else -1
    dt = (t1 - t0) / numsteps
    Q = list(range(q0, q1 + dq, dq))
    T = [t0 + i * dt for i in range(len(Q))]
    V = [q / (2 ** 16) * 20 for q in Q]
    return T, V

def sin(amp: float, dc: float, freq: float, phase: float, t0: float,
        t1: float) -> (list[float], list[float]):
    """
    Alias for `sine`.
    """
    return sine(amp, dc, freq, phase, t0, t1)

def sine(amp: float, dc: float, freq: float, phase: float, t0: float,
        t1: float) -> (list[float], list[float]):
    """
    Generate times and voltages for a sine wave of specified amplitude,
    DC offset, frequency, and phase shift from time t0 to time t1 at maximum 
    voltage resolution. The function is defined relative to t0; i.e. the voltage
    at time t=5 for t0=5 and phase=0 is 0 V.

    Parameters
    ----------
    amp, dc, freq, phase : float
        Amplitude, offset, frequency, and phase for the wave.
    t0, t1 : float
        Starting and ending times.

    Returns
    -------
    T, V : list[float]
    """
    # generate all output values
    qmax = int((dc + amp) / 20 * (2 ** 16))
    qmin = int((dc - amp) / 20 * (2 ** 16))
    qvals = list(range(qmin, qmax + 1))

    # assemble one complete cycle of values
    qcycle = qvals[len(qvals) // 2:-1] \
            + qvals[-1:-len(qvals):-1] \
            + qvals[:len(qvals) // 2]

    # find correct times for each output value in the cycle
    tcycle = [
        [math.asin((2 * q - (qmax + qmin)) / (qmax - qmin))
            for q in qcycle[:len(qcycle) // 4]],
        [math.pi / 2 - math.asin((2 * q - (qmax + qmin)) / (qmax - qmin))
            for q in qcycle[len(qcycle) // 4:3 * len(qcycle) // 4]],
        [math.pi / 2 + math.asin((2 * q - (qmax + qmin)) / (qmax - qmin))
            for q in qcycle[3 * len(qcycle) // 4:]]
    ]
    tcycle = tcycle[0] \
            + [math.pi / 2 + t for t in tcycle[1]] \
            + [math.pi * 3 / 2 + t for t in tcycle[2]]

    # adjust times for frequency and initial value apply the correct phase shift
    shift = int((phase % (2 * math.pi)) / (2 * math.pi) * len(qcycle))
    qcycle = qcycle[shift:] + qcycle[:shift]
    dt = [(tcycle[i + 1] - tcycle[i]) / (2 * math.pi * freq)
        for i in range(len(tcycle) - 1)]
    dt = dt[shift:] + dt[:shift]
    acc = 0
    tcycle = [t0]
    for i in range(len(dt)):
        acc += dt[i]
        tcycle.append(t0 + acc)
    
    # assemble full signal
    ncycles = (t1 - t0) * freq
    Q = int(ncycles) * qcycle + qcycle[:int((ncycles % 1) * len(qcycle))]
    V = [q / (2 ** 16) * 20 for q in Q]
    T = [t for t in tcycle]
    for k in range(1, int(ncycles)):
        T += [k / freq + t for t in tcycle]
    T += [int(ncycles) / freq + t
        for t in tcycle[:int((ncycles % 1) * len(tcycle))]]
    return T, V

def cos(amp: float, dc: float, freq: float, phase: float, t0: float,
        t1: float) -> (list[float], list[float]):
    """
    Alias for `cosine`.
    """
    return cosine(amp, dc, freq, phase, t0, t1)

def cosine(amp: float, dc: float, freq: float, phase: float, t0: float,
        t1: float) -> (list[float], list[float]):
    """
    Generate times and voltages for a cosine wave of specified amplitude,
    DC offset, frequency, and phase shift from time t0 to time t1 at maximum 
    voltage resolution. The function is defined relative to t0; i.e. the voltage
    at time t=5 for t0=5 phase=0 and amp=1 is 1 V.

    Parameters
    ----------
    amp, dc, freq, phase : float
        Amplitude, offset, frequency, and phase for the wave.
    t0, t1 : float
        Starting and ending times.

    Returns
    -------
    T, V : list[float]
    """
    return sine(amp, dc, freq, phase + math.pi / 2, t0, t1)

def digital_ramp(S0: int, S1: int, offset: int, t0: float, t1: float) \
        -> (list[float], list[int]):
    """
    Generate times and bits for a digital ramp from state S0 at time t0 to S1 at
    t1 on as many bits as necessary with given offset.

    Parameters
    ----------
    S0, S1 : int
        Starting and ending states.
    offset : int
        Offset (in bits) to apply to the ramp.
    t0, t1 : float
        Starting and ending times.

    Returns
    -------
    T : list[float]
    S : list[int]
    """
    if S1 == S0:
        return [t0, t1], [S0, S1]
    dS = +1 if S1 > S0 else -1
    S = list(range(S0, S1 + dS, dS))
    dt = (t1 - t0) / (len(S) - 1)
    T = [t0 + dt * i for i in range(len(S))]
    return T, [s << offset for s in S]

