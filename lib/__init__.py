# import lib.entangleware_control_link as control_link
# import lib.entangleware_math as math
from .structures import (
    UNSHIFT_ANALOG,
    EventType,
    EVENTPROPS,
    IEVENTPROPS,
    Event,
    Sequence,
    Connection,
    DigitalConnection,
    AnalogConnection,
    ConnectionLayout,
    Computer,
    ch
)
from .sequences import SuperSequence

