from setuptools import setup
from setuptools_rust import Binding, RustExtension
import toml

cargo_file = "Cargo.toml"
cargo = toml.load(cargo_file)
setup_requires = [
    "setuptools",
    "setuptools-rust"
]

source_files = [
]
extensions = [
    RustExtension(
        (X
            .replace("lib_rs/", cargo["lib"]["name"]+".")
            .replace("/", ".")
            .replace(".rs", "")
        ),
        path=cargo_file,
        binding=Binding.PyO3,
        debug=False
    ) for X in source_files
]

setup(
    name=cargo["package"]["name"],
    version=cargo["package"]["version"],
    packages=[cargo["lib"]["name"]],
    rust_extensions=extensions,
    setup_requires=setup_requires,
    zip_safe=False
)
   
