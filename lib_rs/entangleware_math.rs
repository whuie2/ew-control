//! Provides functions to create voltage sequences following mathematical
//! formulae.

use thiserror::Error;

#[derive(Debug, Error)]
pub enum MathError {
    #[error("exponential ramp: rate or time interval is too small")]
    ExpSmallTimestep,
}
pub type MathResult<T> = Result<T, MathError>;

/// This will generate two `Vec<f64>`s containing time data and analog data.
/// This data will be an exponential decay function that will start at (t0, V0)
/// and end at (t1, V1). Negative values for `rate` give a decaying exponential
/// ramp to the endpoint.
///
/// *Returns `MathError::ExpSmallTimestep` if the rate and time interval give
/// time steps too small for the DAC to handle.*
pub fn exponential_ramp(V0: f64, V1: f64, t0: f64, t1: f64, rate: f64)
    -> MathResult<(Vec<f64>, Vec<f64>)>
{
    let q0: i32 = (V0 / 20.0 * 2.0_f64.powi(16)) as i32; // integer DAC values
    let q1: i32 = (V1 / 20.0 * 2.0_f64.powi(16)) as i32;
    if q1 == q0 {
        return Ok((vec![t0, t1], vec![V0, V1]));
    }

    if (rate * (t1 - t0)).abs() < 1.0e-6 {
        return Err(MathError::ExpSmallTimestep);
    }
    let delta: f64 = (rate * (t1 - t0)).exp(); // timestep constant
    let alpha: f64 = ((q0 - q1) as f64) / (1.0 - delta); // dac-step constant
    let offset: f64 = q0 as f64 - alpha;

    let qsteps: Vec<f64> = if q0 < q1 {
        (q0..q1 + 1).map(|q| q as f64).collect()
    } else {
        (q1..q0 + 1).map(|q| q as f64).collect()
    };
    let mut tsteps: Vec<f64> = qsteps.iter()
        .map(|q| t0 + ((q - offset) / alpha).ln() / rate).collect();
    let mut Vsteps: Vec<f64> = qsteps.iter()
        .map(|q| 20.0 * q / 2.0_f64.powi(16)).collect();
    if q0 > q1 {
        tsteps.reverse();
        Vsteps.reverse();
    }
    Ok((tsteps, Vsteps))
}

/// Generate times and voltages for a linear tamp from (t0, V0) to (t1, V1) at
/// maximum (16-bit) voltage resolution.
pub fn linear_ramp(V0: f64, V1: f64, t0: f64, t1: f64) -> (Vec<f64>, Vec<f64>) {
    let q0: i32 = (V0 / 20.0 * 2.0_f64.powi(16)) as i32; // integer DAC values
    let q1: i32 = (V1 / 20.0 * 2.0_f64.powi(16)) as i32;
    if q1 == q0 {
        return (vec![t0, t1], vec![V0, V1]);
    }

    let numsteps: i32 = (q1 - q0).abs();
    let dt: f64 = (t1 - t0) / numsteps as f64;
    let Q: Vec<f64> = if q0 < q1 {
        (q0..q1 + 1).map(|q| q as f64).collect()
    } else {
        (q1..q0 + 1).map(|q| q as f64).rev().collect()
    };
    let T: Vec<f64>
        = Q.iter().enumerate().map(|(k, _)| t0 + k as f64 * dt).collect();
    let V: Vec<f64>
        = Q.iter().map(|q| q / 2.0_f64.powi(16) * 20.0).collect();
    (T, V)
}

// I'm not going to implement sin/cos/digital_ramp because we basically never
// use this module

