//! Provides [`SuperSequence`], the main tool for organizing and saving
//! sequence data.

use std::{
    collections::HashMap,
    fs,
    io::{
        Read,
        Write,
    },
    ops::{
        Deref,
        DerefMut,
        Index,
        IndexMut,
    },
    path::{
        Path,
        PathBuf,
    },
};
use indexmap::IndexMap;
use serde_json as json;
use toml;
use crate::structures::{
    EWError,
    EWResult,
    connection::{
        ConnectionLayout,
        NamedConnection,
    },
    event::Event,
    sequence::{
        ColorCycler,
        SeqColor,
        Sequence,
        NamedSequence,
    },
    state::State,
};

/// Represents a collection of named [`Sequence<Event>`][Sequence]s.
///
/// In addition to sequence data, Each `SuperSequence` carries a collection of
/// named values used to parameterize the sequences.
///
/// This type is mainly to be used for compartmentalization and organization of
/// `Sequence`s at scale and acts as the main interface to the filesystem
/// through [`SuperSequence::save`] and [`SuperSequence::load`].
#[derive(Clone, Debug, PartialEq)]
pub struct SuperSequence {
    sequences: HashMap<String, Sequence<Event>>,
    parameters: HashMap<String, f64>,
}

impl SuperSequence {
    /// Create a new `SuperSequence`.
    pub fn new<I, J>(sequences: I, parameters: J) -> Self
    where
        I: IntoIterator<Item = (String, Sequence<Event>)>,
        J: IntoIterator<Item = (String, f64)>,
    {
        Self {
            sequences: sequences.into_iter().collect(),
            parameters: parameters.into_iter().collect(),
        }
    }

    /// Get a reference to the parameters.
    pub fn get_parameters(&self) -> &HashMap<String, f64> { &self.parameters }

    /// Get the value of a single parameter if it exists.
    pub fn get_parameter(&self, key: &str) -> Option<f64> {
        self.parameters.get(key).copied()
    }

    /// Set or update parameter values.
    pub fn set_parameters<I>(&mut self, params: I) -> &mut Self
    where I: IntoIterator<Item = (String, f64)>
    {
        self.parameters = params.into_iter().collect();
        self
    }

    /// Set or update a single parameter value.
    pub fn set_parameter(&mut self, key: String, val: f64) -> &mut Self {
        self.parameters.insert(key, val);
        self
    }

    /// Set or update parameter values.
    pub fn with_parameters<I>(mut self, params: I) -> Self
    where I: IntoIterator<Item = (String, f64)>
    {
        self.parameters = params.into_iter().collect();
        self
    }

    /// Set or update a single parameter value.
    pub fn with_parameter(mut self, key: String, val: f64) -> Self {
        self.parameters.insert(key, val);
        self
    }

    /// Find the time of the earliest event in `self`, if `self` is not empty.
    pub fn min_time(&self) -> Option<f64> {
        self.sequences.values()
            .filter_map(|seq| seq.min_time())
            .min_by(|l, r| l.total_cmp(r))
    }

    /// Find the time of the latest event in `self`, if `self` is not empty.
    pub fn max_time(&self) -> Option<f64> {
        self.sequences.values()
            .filter_map(|seq| seq.max_time())
            .max_by(|l, r| l.total_cmp(r))
    }

    /// Set the color of a sequence.
    ///
    /// This function has no effect if `self` does not contain the desired
    /// sequence.
    pub fn set_color(&mut self, seq_name: &str, color: Option<SeqColor>) {
        self.sequences.get_mut(seq_name)
            .map(|seq| seq.set_color(color));
    }

    /// Convert `self` into a single [`Sequence`] of [`Event`]s.
    pub fn into_sequence(self) -> Sequence<Event> {
        Sequence::<Event>::joinall(
            self.into_iter().map(|(_, seq)| seq)
        )
    }

    /// Convert `self` into a single [`Sequence`] of [`Event`]s.
    pub fn to_sequence(&self) -> Sequence<Event> {
        Sequence::<Event>::joinall(
            self.values().cloned()
        )
    }

    /// Convert `self` into a single [`Sequence`] of [`State`]s.
    pub fn into_states(self) -> EWResult<Sequence<State>> {
        Ok(
            Sequence::<State>::joinall(
                self.into_iter()
                    .map(|(_, seq)| seq.to_states())
                    .collect::<EWResult<Vec<Sequence<State>>>>()?
            )
        )
    }

    /// Convert `self` into a single [`Sequence`] of [`State`]s.
    pub fn to_states(&self) -> EWResult<Sequence<State>> {
        Ok(
            Sequence::<State>::joinall(
                self.values()
                    .map(|seq| seq.to_states())
                    .collect::<EWResult<Vec<Sequence<State>>>>()?
            )
        )
    }

    /// Write all sequence and parameter data to a `target` directory.
    ///
    /// Sequence data will be stored in `sequence.toml` and parameter data
    /// will be stored in `parameters.toml` inside `target`.
    pub fn save<P>(&self, target: P, printflag: bool)
        -> EWResult<&Self>
    where P: AsRef<Path>
    {
        let target_dir: PathBuf = target.as_ref().to_path_buf();
        if printflag {
            println!(
                "[sequencing] saving sequence data to '{}':",
                target_dir.display(),
            );
        }
        if printflag { println!("[sequencing] save sequence"); }
        // let target_seq: PathBuf = target_dir.join("sequence.toml");
        let target_seq: PathBuf = target_dir.join("sequence.json");
        let sequences_string: String
            // = toml::to_string(&self.sequences)
            = json::to_string_pretty(&self.sequences)
                .map_err(|e| EWError::SerializationError(e.to_string()))?;
        if printflag { println!("[sequencing] save parameters"); }
        let target_par: PathBuf = target_dir.join("parameters.toml");
        // let target_par: PathBuf = target_dir.join("parameters.json");
        let parameters_string: String
            = toml::to_string(&self.parameters)
            // = json::to_string(&self.parameters)
                .map_err(|e| EWError::SerializationError(e.to_string()))?;
        if !target_dir.is_dir() {
            if printflag {
                println!(":: mkdir -p '{}'", target_dir.display());
            }
            fs::create_dir_all(target_dir)
                .map_err(|e| EWError::FilesystemError(e.to_string()))?;
        }
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(target_seq)
            .map_err(|e| EWError::FilesystemError(e.to_string()))?
            .write_all(&sequences_string.into_bytes())
            .map_err(|e| EWError::FilesystemError(e.to_string()))?;
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(target_par)
            .map_err(|e| EWError::FilesystemError(e.to_string()))?
            .write_all(&parameters_string.into_bytes())
            .map_err(|e| EWError::FilesystemError(e.to_string()))?;
        Ok(self)
    }

    /// Load sequence and parameter data from a `target` directory.
    ///
    /// `target` must name a directory containing files `sequence.toml` and
    /// `parameters.toml`.
    pub fn load<P>(
        target: P,
        printflag: bool,
    ) -> EWResult<Self>
    where P: AsRef<Path>
    {
        let target_dir: PathBuf = target.as_ref().to_path_buf();
        if printflag {
            println!(
                "[sequencing] loading data from target '{}':",
                target_dir.display(),
            );
        }
        target_dir.is_dir().then_some(())
            .ok_or(
                EWError::FilesystemError(
                    "target must be an existing directory".to_string()))?;

        let mut buf = String::new();
        if printflag { println!("[sequencing] load sequence"); }
        // let target_seq: PathBuf = target_dir.join("sequence.toml");
        let target_seq: PathBuf = target_dir.join("sequence.json");
        fs::OpenOptions::new()
            .read(true)
            .open(target_seq)
            .map_err(|e| EWError::FilesystemError(e.to_string()))?
            .read_to_string(&mut buf)
            .map_err(|e| EWError::FilesystemError(e.to_string()))?;
        let sequences: HashMap<String, Sequence<Event>>
            // = toml::from_str(&std::mem::take(&mut buf))
            = json::from_str(&std::mem::take(&mut buf))
            .map_err(|e| EWError::DeserializationError(e.to_string()))?;

        if printflag { println!("[sequencing] load parameters"); }
        let target_par: PathBuf = target_dir.join("parameters.toml");
        // let target_par: PathBuf = target_dir.join("parameters.json");
        fs::OpenOptions::new()
            .read(true)
            .open(target_par)
            .map_err(|e| EWError::FilesystemError(e.to_string()))?
            .read_to_string(&mut buf)
            .map_err(|e| EWError::FilesystemError(e.to_string()))?;
        let parameters: HashMap<String, f64>
            = toml::from_str(&buf)
            // = json::from_str(&buf)
            .map_err(|e| EWError::DeserializationError(e.to_string()))?;

        Ok(Self { sequences, parameters })
    }

    /// Compile all sequences to single [`Event`]s and organize into an
    /// [`IndexMap`] where each key-value pair is a [`NamedConnection`] found
    /// in `conn_layout` and a list of [`Sequence`]s (each retaining their name
    /// from `self`) containing only events occurring on that connection. Each
    /// list is sorted by the sequences' earliest times, and each `Sequence` is
    /// guaranteed to be non-empty.
    ///
    /// Optionally pass a [`ColorCycler`] to auto-pick colors for any
    /// `Sequence`s for which a color has not already been set.
    pub fn singles_by_connection(
        &self,
        conn_layout: &ConnectionLayout,
        mut colors: Option<&mut ColorCycler>,
    ) -> IndexMap<NamedConnection, Vec<NamedSequence<Event>>>
    {
        conn_layout.iter()
            .map(|(conn_name, conn)| {
                let mut sequences: Vec<(String, Sequence<Event>)>
                    = self.iter()
                    .filter_map(|(seq_name, seq)| {
                        let with_conn: Sequence<Event>
                            = seq.filter_connection_single(conn);
                        if !with_conn.is_empty() {
                            Some((seq_name.clone(), with_conn))
                        } else {
                            None
                        }
                    })
                    .map(|(seq_name, mut seq)| {
                        seq.color
                            = seq.color.or_else(|| {
                                colors.as_mut().map(|c| c.next().unwrap())
                            });
                        (seq_name, seq)
                    })
                    .collect();
                sequences.sort_by(
                    |(_seq_name_l, seq_l), (_seq_name_r, seq_r)| {
                        seq_l.min_time().unwrap()
                            .total_cmp(&seq_r.min_time().unwrap())
                    }
                );
                ((conn_name.clone(), *conn), sequences)
            })
            .collect()
    }

    /// Compile all sequences into single [`Event`]s and organize into an
    /// [`IndexMap`] where each key-value pair is a [`NamedConnection`] found in
    /// `conn_layout` and a list of [`NamedSequence`]s (each retaining their
    /// name from `self`) containing only events occurring on that connection.
    /// Each list is sorted by the sequences' earliest times and each
    /// [`NamedSequence`] is guaranteed to be non-empty.
    ///
    /// Details are discarded where possible (e.g. from serial events) in order
    /// to reduce the load on the visualizer.
    ///
    /// Optionally pass a [`ColorCycler`] to auto-pick colors for any
    /// `Sequence`s for which a color has not already been set.
    pub fn singles_by_connection_viewer(
        &self,
        conn_layout: &ConnectionLayout,
        mut colors: Option<&mut ColorCycler>,
    ) -> IndexMap<NamedConnection, Vec<NamedSequence<Event>>>
    {
        conn_layout.iter()
            .map(|(conn_name, conn)| {
                let mut sequences: Vec<(String, Sequence<Event>)>
                    = self.iter()
                    .filter_map(|(seq_name, seq)| {
                        let with_conn: Sequence<Event>
                            = seq.filter_connection_single_viewer(conn);
                        if !with_conn.is_empty() {
                            Some((seq_name.clone(), with_conn))
                        } else {
                            None
                        }
                    })
                    .map(|(seq_name, mut seq)| {
                        seq.color
                            = seq.color.or_else(|| {
                                colors.as_mut().map(|c| c.next().unwrap())
                            });
                        (seq_name, seq)
                    })
                    .collect();
                sequences.sort_by(
                    |(_seq_name_l, seq_l), (_seq_name_r, seq_r)| {
                        seq_l.min_time().unwrap()
                            .total_cmp(&seq_r.min_time().unwrap())
                    }
                );
                ((conn_name.clone(), *conn), sequences)
            })
            .collect()
    }
}

impl AsRef<HashMap<String, Sequence<Event>>> for SuperSequence {
    fn as_ref(&self) -> &HashMap<String, Sequence<Event>> {
        &self.sequences
    }
}

impl AsMut<HashMap<String, Sequence<Event>>> for SuperSequence {
    fn as_mut(&mut self) -> &mut HashMap<String, Sequence<Event>> {
        &mut self.sequences
    }
}

impl Deref for SuperSequence {
    type Target = HashMap<String, Sequence<Event>>;

    fn deref(&self) -> &Self::Target { &self.sequences }
}

impl DerefMut for SuperSequence {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.sequences }
}

impl Index<&str> for SuperSequence {
    type Output = Sequence<Event>;

    fn index(&self, index: &str) -> &Sequence<Event> {
        if !self.sequences.contains_key(index) {
            panic!("missing key: {}", index);
        }
        &self.sequences[index]
    }
}

impl IndexMut<&str> for SuperSequence {
    fn index_mut(&mut self, index: &str) -> &mut Sequence<Event> {
        if !self.sequences.contains_key(index) {
            panic!("missing key: {}", index);
        }
        self.sequences.get_mut(index).unwrap()
    }
}

pub struct SuperSequenceIntoIter {
    iter: std::collections::hash_map::IntoIter<String, Sequence<Event>>,
}

impl Iterator for SuperSequenceIntoIter {
    type Item = (String, Sequence<Event>);

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl IntoIterator for SuperSequence {
    type Item = (String, Sequence<Event>);
    type IntoIter = SuperSequenceIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        SuperSequenceIntoIter { iter: self.sequences.into_iter() }
    }
}

pub struct SuperSequenceIter<'a> {
    iter: std::collections::hash_map::Iter<'a, String, Sequence<Event>>,
}

impl<'a> Iterator for SuperSequenceIter<'a> {
    type Item = (&'a String, &'a Sequence<Event>);

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl<'a> IntoIterator for &'a SuperSequence {
    type Item = (&'a String, &'a Sequence<Event>);
    type IntoIter = SuperSequenceIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        SuperSequenceIter { iter: self.sequences.iter() }
    }
}

pub struct SuperSequenceIterMut<'a> {
    iter: std::collections::hash_map::IterMut<'a, String, Sequence<Event>>,
}

impl<'a> Iterator for SuperSequenceIterMut<'a> {
    type Item = (&'a String, &'a mut Sequence<Event>);

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl<'a> IntoIterator for &'a mut SuperSequence {
    type Item = (&'a String, &'a mut Sequence<Event>);
    type IntoIter = SuperSequenceIterMut<'a>;

    fn into_iter(self) -> Self::IntoIter {
        SuperSequenceIterMut { iter: self.sequences.iter_mut() }
    }
}

/// Create a new [`SuperSequence`].
#[macro_export]
macro_rules! supersequence(
    (
        sequences: {
            $( $name:expr => $seq:expr ),* $(,)?
        },
        parameters: {
            $( $param:expr => $val:expr ),* $(,)?
        }
    ) => {
        $crate::structures::supersequence::SuperSequence::new(
            [$(
                ( $name.to_string(), $seq )
            ),*],
            [$(
                ( $param.to_string(), $val )
            ),*]
        )
    }
);

