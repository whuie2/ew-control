//! Low-level descriptions of sequences using only the primitives sent to the
//! Entangleware Control backend.

use crate::{
    entangleware_control_link::{
        CHANNELS_D,
        CHANNELS_A,
        CLError,
    },
    structures::{
        EWResult,
        connection::Connection,
        sequence::Timed,
        voltage::ToVoltage,
    },
};

/// Holds the time and data of a single state in terms of only the primitives
/// eventually sent to Entangleware Control. Usually you'll want to use
/// [`Event`][crate::structures::event::Event] instead.
///
/// Digital states define a global state for all 32 channels housed in a single
/// connector on the NI digital board, of which there are 4. Individual
/// channels' states are set to HIGH or LOW according to the result of a
/// bit-wise AND between `mask` (first of the `Digital` variant's
/// `u32` values) and `state` 32-bit integers, where a 1 in the `k`-th position
/// (`k` starting from 0 on the right) corresponds to HIGH on the `k`-th channel
/// of the connector.
///
/// Since there is only one connector on the NI analog board, analog states
/// define a local state of only a single channel, of which there are 32 total.
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum State {
    Digital(
        // /// board
        // u8,
        /// connector
        u8,
        /// mask
        u32,
        /// state
        u32,
        /// time
        f64,
    ),
    Analog(
        // /// board
        // u8,
        /// channel
        u8,
        /// state
        f64,
        /// time
        f64,
    ),
}

impl State {
    /// Create a new state setting on the given [`Connection`].
    pub fn new<V>(conn: Connection, state: V, time: f64, with_delay: bool)
        -> EWResult<Self>
    where V: ToVoltage
    {
        match conn {
            Connection::Digital(c, ch, _, del_up, del_dn) => {
                (ch < CHANNELS_D).then_some(())
                    .ok_or(CLError::ChannelOutOfRange(ch))?;
                let mask: u32 = if ch < 32 { 1_u32 << ch } else { 0_u32 };
                let state: u32 = state.for_conn(&conn).into();
                let state_bits: u32 = if ch < 32 { state << ch } else { 0_u32 };
                let time: f64
                    = if with_delay {
                        if state > 0 { time - del_up } else { time - del_dn }
                    } else {
                        time
                    };
                Ok(Self::Digital(c, mask, state_bits, time))
            },
            Connection::Analog(ch, _, del) => {
                (ch < CHANNELS_A).then_some(())
                    .ok_or(CLError::ChannelOutOfRange(ch))?;
                let state: f64 = state.for_conn(&conn).into();
                let time: f64 = if with_delay { time - del } else { time };
                Ok(Self::Analog(ch, state, time))
            },
        }
    }

    /// Get the connector number if `self` is `Digital`.
    pub fn get_connector(&self) -> Option<u8> {
        match self {
            State::Digital(c, _, _, _) => Some(*c),
            State::Analog(_, _, _) => None,
        }
    }

    /// Get the channel number if `self` is `Analog`.
    pub fn get_channel(&self) -> Option<u8> {
        match self {
            State::Digital(_, _, _, _) => None,
            State::Analog(ch, _, _) => Some(*ch),
        }
    }

    /// Get the integer describing the digital event state (mask included) if
    /// `self` is `Digital`.
    pub fn get_state_d(&self) -> Option<u32> {
        match self {
            State::Digital(_, m, s, _) => Some(m & s),
            State::Analog(_, _, _) => None,
        }
    }

    /// Get the analog event voltage if `self` is `Analog`.
    pub fn get_state_a(&self) -> Option<f64> {
        match self {
            State::Digital(_, _, _, _) => None,
            State::Analog(_, s, _) => Some(*s),
        }
    }

    /// Get the event time.
    pub fn get_time(&self) -> f64 {
        match self {
            State::Digital(_, _, _, t) => *t,
            State::Analog(_, _, t) => *t,
        }
    }
}

impl Timed for State {
    fn min_time(&self) -> f64 { self.get_time() }

    fn max_time(&self) -> f64 { self.get_time() }
}

