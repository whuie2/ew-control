//! Provides [`Sequence<T>`][Sequence], a sugared [`Vec`] of sequence elements
//! with an associated color for plotting.
//!
//! Types held by a `Sequence` must implement [`Timed`], which requires that
//! each element provide a (possibly singular) range of times over which the
//! element occurs.

use std::{
    ops::{
        Add,
        AddAssign,
        Deref,
        DerefMut,
        Shl,
        ShlAssign,
    },
};
use serde::{
    Deserialize,
    Serialize,
};
use crate::structures::{
    EWResult,
    connection::Connection,
    event::Event,
    state::State,
};

/// Describes an element of a sequence occurring over some time.
pub trait Timed: Clone + PartialEq {
    /// Earliest time.
    fn min_time(&self) -> f64;

    /// Latest time.
    fn max_time(&self) -> f64;

    /// Shorthand for `(self.min_time(), self.max_time())`.
    fn when(&self) -> (f64, f64) { (self.min_time(), self.max_time()) }
}

/// Describes a series of partially time-orderable elements occuring in
/// sequence.
///
/// This type will usually hold [`Event`]s and [`State`]s: `Sequence<Event>`s
/// are used for serialization and visualization, while `Sequence<State>`s are
/// treated as the "compiled" versions of `Sequence<Event>`s to be fed to a
/// [`Computer`][crate::structures::computer::Computer] for physical
/// realization. Every `Sequence` can be (mutably) dereferenced to its
/// underlying `Vec` (discarding its color).
///
/// This type also overloads the `+`, `+=`, `<<`, and `<<=` operators for
/// pushing and appending operations:
/// - `Sequence<T> + Sequence<T> -> Sequence<T>` (concatenate)
/// - `&Sequence<T> + &Sequence<T> -> Sequence<T>` (concatenate with clone)
/// - `Sequence<T> += Sequence<T>` (append)
/// - `Sequence<T> += &Sequence<T>` (append with clone)
/// - `Sequence<T> << T -> Sequence<T>` (push expression)
/// - `Sequence<T> << Sequence<T> -> Sequence<T>` (concatenate)
/// - `Sequence<T> <<= T` (push)
/// - `Sequence<T> <<= Sequence<T>` (append)
///
/// For easy `Sequence` creation, the following are implemented for `Event` and
/// `State` only:
/// - `T << T -> Sequence<T>` (create)
/// - `T << Sequence<T> -> Sequence<T>` (prepend)
///
/// This allows `Sequence`s of only a few `Event`s/`State`s to be conveniently
/// created with only `<<` expressions strung together.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Sequence<T>
where T: Timed
{
    data: Vec<T>,
    pub color: Option<SeqColor>,
}

impl<T> PartialEq for Sequence<T>
where T: Timed
{
    fn eq(&self, other: &Self) -> bool { self.data == other.data }
}

impl<T> Default for Sequence<T>
where T: Timed
{
    fn default() -> Self { Self { data: Vec::new(), color: None } }
}

impl<T> Sequence<T>
where T: Timed
{
    /// Create a new, colorless `Sequence`.
    pub fn new<I>(data: I) -> Self
    where I: IntoIterator<Item = T>
    {
        Self {
            data: data.into_iter().collect(),
            color: None,
        }
    }

    /// Set the color for plotting, consuming `self`.
    pub fn with_color(mut self, color: Option<SeqColor>) -> Self {
        self.color = color;
        self
    }

    /// Set the color for plotting.
    pub fn set_color(&mut self, color: Option<SeqColor>) -> &mut Self {
        self.color = color;
        self
    }

    /// Join `self` with `other`, consuming `other`. `self`'s color is
    /// retained.
    pub fn join(&mut self, mut other: Self) -> &mut Self {
        self.data.append(&mut other.data);
        self
    }

    /// Create a new, colorless `Sequence` by joining a series of `Sequence`s
    /// together.
    pub fn joinall<I, J, U>(sequences: I) -> Sequence<U>
    where
        I: IntoIterator<Item = J>,
        J: IntoIterator<Item = U>,
        U: Timed,
    {
        sequences.into_iter()
            .flat_map(|seq| seq.into_iter())
            .collect()
    }

    /// Find the minimum element time if `self` is not empty.
    pub fn min_time(&self) -> Option<f64> {
        self.data.iter()
            .map(|item| item.min_time())
            .min_by(|l, r| l.total_cmp(r))
    }

    /// Find the maximum element time if `self` is not empty.
    pub fn max_time(&self) -> Option<f64> {
        self.data.iter()
            .map(|item| item.max_time())
            .max_by(|l, r| l.total_cmp(r))
    }

    /// Shorthand for `(self.min_time(), self.max_time())`.
    pub fn when(&self) -> Option<(f64, f64)> {
        self.min_time()
            .and_then(|tmin| self.max_time().map(|tmax| (tmin, tmax)))
    }
}

impl Sequence<Event> {
    // /// Create a new, colorless `Sequence` of digital events from an arbitrary
    // /// series of `(time, voltage)` pairs.
    // pub fn from_digital_data<I, V>(conn: Connection, data: I, with_delay: bool)
    //     -> EWResult<Self>
    // where
    //     I: IntoIterator<Item = (f64, V)>,
    //     V: ToVoltage,
    // {
    //     data.into_iter()
    //         .map(|(t, v)| Event::new_digital(conn, v, t, with_delay))
    //         .collect()
    // }
    //
    // /// Create a new, colorless `Sequence` of digital serial events from an
    // /// arbitrary series of `(time, value)` pairs.
    // #[allow(clippy::too_many_arguments)]
    // pub fn from_serial_data<I>(
    //     conn: Connection,
    //     conn_clk: Option<Connection>,
    //     conn_sync: Option<Connection>,
    //     data: I,
    //     len: u8,
    //     reg: u32,
    //     len_reg: u8,
    //     with_delay: bool,
    // ) -> EWResult<Self>
    // where I: IntoIterator<Item = (f64, u32)>
    // {
    //     data.into_iter()
    //         .map(|(t, v)| {
    //             Event::new_serial(
    //                 (conn, conn_clk, conn_sync),
    //                 (v, len),
    //                 (reg, len_reg),
    //                 t,
    //                 with_delay,
    //             )
    //         })
    //         .collect()
    // }
    //
    // /// Create a new, colorless `Sequence` of analog events from an arbitrary
    // /// series of `(time, voltage)` pairs.
    // pub fn from_analog_data<I, V>(conn: Connection, data: I, with_delay: bool)
    //     -> EWResult<Self>
    // where
    //     I: IntoIterator<Item = (f64, V)>,
    //     V: ToVoltage,
    // {
    //     data.into_iter()
    //         .map(|(t, v)| Event::new_analog(conn, v, t, with_delay))
    //         .collect()
    // }

    /// Return a `Sequence<Event>` with only single [`Event`]s.
    pub fn to_singles(&self) -> Sequence<Event> {
        self.iter()
            .flat_map(|e| e.to_singles())
            .collect::<Sequence<Event>>()
            .with_color(self.color)
    }

    /// Return a `Sequence<Event>` with only simple single [`Event`]s.
    pub fn to_singles_viewer(&self) -> Sequence<Event> {
        self.iter()
            .flat_map(|e| e.to_singles_viewer())
            .collect::<Sequence<Event>>()
            .with_color(self.color)
    }

    /// Return `true` if every contained [`Event`] is single.
    pub fn is_singles(&self) -> bool {
        self.data.iter().all(|event| event.is_single())
    }

    /// Convert `self` to singles and filter down to only the events on the
    /// given connection.
    pub fn filter_connection_single(&self, conn: &Connection)
        -> Sequence<Event>
    {
        self.to_singles()
            .into_iter()
            .filter(|e| e.get_connection_single().is_some_and(|c| &c == conn))
            .collect::<Sequence<Event>>()
            .with_color(self.color)
    }

    /// Convert `self` to simplified singles (i.e. with
    /// [`Self::to_singles_viewer`]) and filter down to only the events on the
    /// given connection.
    pub fn filter_connection_single_viewer(&self, conn: &Connection)
        -> Sequence<Event>
    {
        self.to_singles_viewer()
            .into_iter()
            .filter(|e| e.get_connection_single().is_some_and(|c| &c == conn))
            .collect::<Sequence<Event>>()
            .with_color(self.color)
    }

    /// Compile all [`Event`]s to a single `Sequence<State>`.
    pub fn to_states(&self) -> EWResult<Sequence<State>> {
        let state_seqs: Vec<Sequence<State>>
            = self.iter()
            .map(|e| e.to_states())
            .collect::<EWResult<Vec<Sequence<State>>>>()?;
        Ok(Self::joinall(state_seqs).with_color(self.color))
    }
}

impl<T> From<Vec<T>> for Sequence<T>
where T: Timed
{
    fn from(v: Vec<T>) -> Self { Self { data: v, color: None } }
}

impl<T, const N: usize> From<[T; N]> for Sequence<T>
where T: Timed
{
    fn from(v: [T; N]) -> Self {
        Self { data: v.into_iter().collect(), color: None }
    }
}

impl<T> AsRef<Vec<T>> for Sequence<T>
where T: Timed
{
    fn as_ref(&self) -> &Vec<T> { &self.data }
}

impl<T> AsMut<Vec<T>> for Sequence<T>
where T: Timed
{
    fn as_mut(&mut self) -> &mut Vec<T> { &mut self.data }
}

impl<T> Deref for Sequence<T>
where T: Timed
{
    type Target = Vec<T>;

    fn deref(&self) -> &Self::Target { &self.data }
}

impl<T> DerefMut for Sequence<T>
where T: Timed
{
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.data }
}

impl<T> FromIterator<T> for Sequence<T>
where T: Timed
{
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = T>
    {
        Self { data: iter.into_iter().collect(), color: None }
    }
}

pub struct SequenceIntoIter<T>
where T: Timed
{
    iter: std::vec::IntoIter<T>,
}

impl<T> Iterator for SequenceIntoIter<T>
where T: Timed
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl<T> IntoIterator for Sequence<T>
where T: Timed
{
    type Item = T;
    type IntoIter = SequenceIntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        SequenceIntoIter { iter: self.data.into_iter() }
    }
}

pub struct SequenceIter<'a, T>
where T: Timed
{
    iter: std::slice::Iter<'a, T>,
}

impl<'a, T> Iterator for SequenceIter<'a, T>
where T: Timed
{
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl<'a, T> IntoIterator for &'a Sequence<T>
where T: Timed
{
    type Item = &'a T;
    type IntoIter = SequenceIter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        SequenceIter { iter: self.data.iter() }
    }
}

impl<T> Add<Sequence<T>> for Sequence<T>
where T: Timed
{
    type Output = Sequence<T>;

    fn add(mut self, rhs: Sequence<T>) -> Self::Output {
        self.data = [self.data, rhs.data].concat();
        self
    }
}

impl<'a, T> Add<&'a Sequence<T>> for &'a Sequence<T>
where T: Timed
{
    type Output = Sequence<T>;

    fn add(self, rhs: &'a Sequence<T>) -> Self::Output {
        self.clone() + rhs.clone()
    }
}

impl<T> AddAssign<Sequence<T>> for Sequence<T>
where T: Timed
{
    fn add_assign(&mut self, mut rhs: Sequence<T>) {
        self.data.append(&mut rhs.data);
    }
}

impl<'a, T> AddAssign<&'a Sequence<T>> for Sequence<T>
where T: Timed
{
    fn add_assign(&mut self, rhs: &'a Sequence<T>) {
        self.data.append(&mut rhs.data.clone());
    }
}

impl<T> Shl<T> for Sequence<T>
where T: Timed
{
    type Output = Sequence<T>;

    fn shl(mut self, rhs: T) -> Self::Output {
        self.data.push(rhs);
        self
    }
}

impl<T> ShlAssign<T> for Sequence<T>
where T: Timed
{
    fn shl_assign(&mut self, rhs: T) {
        self.data.push(rhs);
    }
}

impl<T> ShlAssign<T> for &mut Sequence<T>
where T: Timed
{
    fn shl_assign(&mut self, rhs: T) {
        self.data.push(rhs);
    }
}

impl<T> Shl<Sequence<T>> for Sequence<T>
where T: Timed
{
    type Output = Sequence<T>;

    fn shl(mut self, rhs: Sequence<T>) -> Self::Output {
        self.data = [self.data, rhs.data].concat();
        self
    }
}

impl<T> ShlAssign<Sequence<T>> for Sequence<T>
where T: Timed
{
    fn shl_assign(&mut self, mut rhs: Sequence<T>) {
        self.data.append(&mut rhs.data);
    }
}

impl<T> ShlAssign<Sequence<T>> for &mut Sequence<T>
where T: Timed
{
    fn shl_assign(&mut self, mut rhs: Sequence<T>) {
        self.data.append(&mut rhs.data);
    }
}

impl Shl<Event> for Event {
    type Output = Sequence<Event>;

    fn shl(self, rhs: Event) -> Self::Output {
        vec![self, rhs].into()
    }
}

impl Shl<Sequence<Event>> for Event {
    type Output = Sequence<Event>;

    fn shl(self, mut rhs: Sequence<Event>) -> Self::Output {
        rhs.data.insert(0, self);
        rhs
    }
}

impl Shl<State> for State {
    type Output = Sequence<State>;

    fn shl(self, rhs: State) -> Self::Output {
        vec![self, rhs].into()
    }
}

impl Shl<Sequence<State>> for State {
    type Output = Sequence<State>;

    fn shl(self, mut rhs: Sequence<State>) -> Self::Output {
        rhs.data.insert(0, self);
        rhs
    }
}

pub type NamedSequence<T> = (String, Sequence<T>);

/// RGB color for a sequence.
///
/// This type offers several associated constants for easy access.
///
/// See also [`ColorCycle`] and [`ColorCycler`].
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct SeqColor(pub u8, pub u8, pub u8);

impl SeqColor {
    /// Basic red
    pub const RED: SeqColor     = SeqColor(255,   0,   0);
    /// Basic red
    pub const R: SeqColor       = SeqColor(255,   0,   0);
    /// Basic green
    pub const GREEN: SeqColor   = SeqColor(  0, 180,   0);
    /// Basic green
    pub const G: SeqColor       = SeqColor(  0, 180,   0);
    /// Basic blue
    pub const BLUE: SeqColor    = SeqColor(  0,   0, 255);
    /// Basic blue
    pub const B: SeqColor       = SeqColor(  0,   0, 255);
    /// Basic cyan
    pub const CYAN: SeqColor    = SeqColor(  0, 191, 191);
    /// Basic cyan
    pub const C: SeqColor       = SeqColor(  0, 191, 191);
    /// Basic magenta
    pub const MAGENTA: SeqColor = SeqColor(191,   0, 191);
    /// Basic magenta
    pub const M: SeqColor       = SeqColor(191,   0, 191);
    /// Basic yellow
    pub const YELLOW: SeqColor  = SeqColor(255, 200,   0);
    /// Basic yellow
    pub const Y: SeqColor       = SeqColor(255, 200,   0);
    /// Basic white
    pub const WHITE: SeqColor   = SeqColor(255, 255, 255);
    /// Basic white
    pub const W: SeqColor       = SeqColor(255, 255, 255);
    /// Basic black
    pub const BLACK: SeqColor   = SeqColor(  0,   0,   0);
    /// Basic black
    pub const K: SeqColor       = SeqColor(  0,   0,   0);

    /// Default color cycle item 0 (blue)
    pub const C0: SeqColor = SeqColor( 31, 119, 180);
    /// Default color cycle item 1 (auburn)
    pub const C1: SeqColor = SeqColor(217,  83,  25);
    /// Default color cycle item 2 (canary)
    pub const C2: SeqColor = SeqColor(237, 177,  32);
    /// Default color cycle item 3 (purple)
    pub const C3: SeqColor = SeqColor(126,  47, 142);
    /// Default color cycle item 4 (cyan)
    pub const C4: SeqColor = SeqColor( 70, 173, 217);
    /// Default color cycle item 5 (tangerine)
    pub const C5: SeqColor = SeqColor(255, 127,  14);
    /// Default color cycle item 6 (dark seafoam)
    pub const C6: SeqColor = SeqColor( 61, 120, 110);
    /// Default color cycle item 7 (gray)
    pub const C7: SeqColor = SeqColor(150, 150, 150);
    /// Default color cycle item 8 (burgundy)
    pub const C8: SeqColor = SeqColor(162,  20,  47);
    /// Default color cycle item 9 (dark rose)
    pub const C9: SeqColor = SeqColor(191, 120, 120);

    /// Matplotlib color cycle item 0 (blue)
    pub const MPL_C0: SeqColor = SeqColor( 31, 119, 180);
    /// Matplotlib color cycle item 1 (orange)
    pub const MPL_C1: SeqColor = SeqColor(255, 127,  14);
    /// Matplotlib color cycle item 2 (green)
    pub const MPL_C2: SeqColor = SeqColor( 44, 160,  44);
    /// Matplotlib color cycle item 3 (red)
    pub const MPL_C3: SeqColor = SeqColor(214,  39,  40);
    /// Matplotlib color cycle item 4 (purple)
    pub const MPL_C4: SeqColor = SeqColor(148, 103, 189);
    /// Matplotlib color cycle item 5 (brown)
    pub const MPL_C5: SeqColor = SeqColor(140,  86,  75);
    /// Matplotlib color cycle item 6 (pink)
    pub const MPL_C6: SeqColor = SeqColor(227, 119, 194);
    /// Matplotlib color cycle item 7 (gray)
    pub const MPL_C7: SeqColor = SeqColor(127, 127, 127);
    /// Matplotlib color cycle item 8 (yellow)
    pub const MPL_C8: SeqColor = SeqColor(188, 189,  34);
    /// Matplotlib color cycle item 9 (cyan)
    pub const MPL_C9: SeqColor = SeqColor( 23, 190, 207);

    /// Plotly color cycle item 0 (blue)
    pub const PTL_C0: SeqColor = SeqColor( 99, 110, 250);
    /// Plotly color cycle item 1 (red)
    pub const PTL_C1: SeqColor = SeqColor(239,  85,  59);
    /// Plotly color cycle item 2 (green)
    pub const PTL_C2: SeqColor = SeqColor(  0, 204, 150);
    /// Plotly color cycle item 3 (purple)
    pub const PTL_C3: SeqColor = SeqColor(171,  99, 250);
    /// Plotly color cycle item 4 (orange)
    pub const PTL_C4: SeqColor = SeqColor(255, 161,  90);
    /// Plotly color cycle item 5 (cyan)
    pub const PTL_C5: SeqColor = SeqColor( 25, 211, 243);
    /// Plotly color cycle item 6 (bubblegum)
    pub const PTL_C6: SeqColor = SeqColor(255, 102, 146);
    /// Plotly color cycle item 7 (lime)
    pub const PTL_C7: SeqColor = SeqColor(182, 232, 128);
    /// Plotly color cycle item 8 (pink)
    pub const PTL_C8: SeqColor = SeqColor(255, 151, 255);
    /// Plotly color cycle item 9 (yellow)
    pub const PTL_C9: SeqColor = SeqColor(254, 203,  82);

    /// Matlab color cycle item 0 (blue)
    pub const ML_C0: SeqColor = SeqColor(  0, 114, 189);
    /// Matlab color cycle item 0 (red)
    pub const ML_C1: SeqColor = SeqColor(217,  83,  25);
    /// Matlab color cycle item 0 (yellow)
    pub const ML_C2: SeqColor = SeqColor(237, 177,  32);
    /// Matlab color cycle item 0 (purple)
    pub const ML_C3: SeqColor = SeqColor(126,  47, 142);
    /// Matlab color cycle item 0 (green)
    pub const ML_C4: SeqColor = SeqColor(119, 172,  48);
    /// Matlab color cycle item 0 (cyan)
    pub const ML_C5: SeqColor = SeqColor( 77, 190, 238);
    /// Matlab color cycle item 0 (dark red)
    pub const ML_C6: SeqColor = SeqColor(162,  20,  47);

    /// Get the `k`-th item of the default color cycle.
    pub fn c(k: usize) -> Self {
        match k % 10 {
            0 => Self::C0,
            1 => Self::C1,
            2 => Self::C2,
            3 => Self::C3,
            4 => Self::C4,
            5 => Self::C5,
            6 => Self::C6,
            7 => Self::C7,
            8 => Self::C8,
            9 => Self::C9,
            _ => unreachable!(),
        }
    }

    /// Get the `k`-th item of the Matplotlib color cycle.
    pub fn mpl_c(k: usize) -> Self {
        match k % 10 {
            0 => Self::MPL_C0,
            1 => Self::MPL_C1,
            2 => Self::MPL_C2,
            3 => Self::MPL_C3,
            4 => Self::MPL_C4,
            5 => Self::MPL_C5,
            6 => Self::MPL_C6,
            7 => Self::MPL_C7,
            8 => Self::MPL_C8,
            9 => Self::MPL_C9,
            _ => unreachable!(),
        }
    }

    /// Get the `k`-th item of the Plotly color cycle.
    pub fn ptl_c(k: usize) -> Self {
        match k % 10 {
            0 => Self::PTL_C0,
            1 => Self::PTL_C1,
            2 => Self::PTL_C2,
            3 => Self::PTL_C3,
            4 => Self::PTL_C4,
            5 => Self::PTL_C5,
            6 => Self::PTL_C6,
            7 => Self::PTL_C7,
            8 => Self::PTL_C8,
            9 => Self::PTL_C9,
            _ => unreachable!(),
        }
    }

    /// Get the `k`-th item of the Matlab color cycle.
    pub fn ml_c(k: usize) -> Self {
        match k % 7 {
            0 => Self::ML_C0,
            1 => Self::ML_C1,
            2 => Self::ML_C2,
            3 => Self::ML_C3,
            4 => Self::ML_C4,
            5 => Self::ML_C5,
            6 => Self::ML_C6,
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum ColorCycle {
    Default,
    Matplotlib,
    Plotly,
    Matlab,
}

/// Generator of [`SeqColor`]s for a given [`ColorCycle`].
///
/// This type is a never-ending [`Iterator`] and will always yield a color in
/// its cycle, regardless of the number of times `next` has been called.
#[derive(Clone, Debug)]
pub struct ColorCycler {
    cycle: ColorCycle,
    k: usize,
    N: usize,
}

impl ColorCycler {
    /// Create a new `ColorCycler` for a given [`ColorCycle`].
    pub fn new(cycle: ColorCycle) -> Self {
        match cycle {
            ColorCycle::Default => Self { cycle, k: 0, N: 10 },
            ColorCycle::Matplotlib => Self { cycle, k: 0, N: 10 },
            ColorCycle::Plotly => Self { cycle, k: 0, N: 10 },
            ColorCycle::Matlab => Self { cycle, k: 0, N: 7 },
        }
    }
}

impl Iterator for ColorCycler {
    type Item = SeqColor;

    fn next(&mut self) -> Option<Self::Item> {
        let color
            = match self.cycle {
                ColorCycle::Default => SeqColor::c(self.k),
                ColorCycle::Matplotlib => SeqColor::mpl_c(self.k),
                ColorCycle::Plotly => SeqColor::ptl_c(self.k),
                ColorCycle::Matlab => SeqColor::ml_c(self.k),
            };
        self.k = (self.k + 1) % self.N;
        Some(color)
    }
}

