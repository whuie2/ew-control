//! High-level descriptions of sequence elements occurring over a number of
//! [`Connection`]s.
//!
//! These can be simple, like setting the voltage on a single analog channel,
//! or more complicated, like a ramp in a value controlled by a value that's
//! transmitted serially using multiple digital channels. Every [`Event`] can
//! be "compiled" down to either `Event` "singles" (single-channel, single-time
//! digital or analog events) or [`State`]s, which hold only the primitives
//! that are communicated to the Entangleware Control backend.

use std::ops::Range;
use serde::{
    Serialize,
    Deserialize,
};
use crate::structures::{
    to_bits,
    EWError,
    EWResult,
    connection::{
        Connection,
        ConnectionType,
    },
    sequence::{
        Sequence,
        Timed,
    },
    state::State,
    voltage::{
        DigitalState,
        ToVoltage,
        Voltage,
    },
};

/// Clock frequency for [`Event::Serial*`][Event] events.
pub const SERIAL_CLOCK_FREQ: f64 = 1e6;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub(crate) enum EventData {
    Digital {
        conn: Connection,
        state: DigitalState,
        time: f64,
        with_delay: bool,
    },
    DigitalData {
        conn: Connection,
        data: Vec<(f64, DigitalState)>,
        with_delay: bool,
    },
    Pulse {
        conn: Connection,
        inverted: bool,
        time_beg: f64,
        time_end: f64,
        with_delay: bool,
    },
    Serial {
        conn: Connection,
        conn_clk: Option<Connection>,
        conn_sync: Option<Connection>,
        val: u32,
        len: u8,
        reg: u32,
        len_reg: u8,
        time: f64,
        with_delay: bool,
    },
    SerialRamp {
        conn: Connection,
        conn_clk: Option<Connection>,
        conn_sync: Option<Connection>,
        val_beg: u32,
        val_end: u32,
        len: u8,
        reg: u32,
        len_reg: u8,
        time_beg: f64,
        time_end: f64,
        n_steps: usize,
        with_delay: bool,
    },
    SerialData {
        conn: Connection,
        conn_clk: Option<Connection>,
        conn_sync: Option<Connection>,
        data: Vec<(f64, u32)>,
        len: u8,
        reg: u32,
        len_reg: u8,
        with_delay: bool,
    },
    Analog {
        conn: Connection,
        val: f64,
        time: f64,
        with_delay: bool,
    },
    AnalogRamp {
        conn: Connection,
        val_beg: f64,
        val_end: f64,
        time_beg: f64,
        time_end: f64,
        n_steps: usize,
        with_delay: bool,
    },
    AnalogData {
        conn: Connection,
        data: Vec<(f64, f64)>,
        with_delay: bool,
    },
}

/// Description of a message passed via [`Event::serial`].
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct SerialMessage {
    /// Integer value of the message.
    val: u32,
    /// Length, in bits, of the message.
    len: u8,
}

impl<T, U> From<(T, U)> for SerialMessage
where
    T: Into<u32>,
    U: Into<u8>,
{
    fn from(msg: (T, U)) -> Self {
        let (val, len) = msg;
        Self { val: val.into(), len: len.into() }
    }
}

/// A group of up to three channels used for [`Event::serial`].
#[derive(Copy, Clone, Debug)]
pub struct SerialConnections {
    /// Main signal channel, on which bits are transmitted.
    sig: Connection,
    /// Clock channel, fixing timing for the signal channel.
    clk: Option<Connection>,
    /// Sync channel, indicating when to expect the signal.
    sync: Option<Connection>,
}

impl <T, U, V> From<(T, U, V)> for SerialConnections
where
    T: Into<Connection>,
    U: Into<Connection>,
    V: Into<Connection>,
{
    fn from(bundle: (T, U, V)) -> Self {
        let (c1, c2, c3) = bundle;
        Self {
            sig: c1.into(),
            clk: Some(c2.into()),
            sync: Some(c3.into()),
        }
    }
}

impl <T, U, V> From<(T, Option<U>, V)> for SerialConnections
where
    T: Into<Connection>,
    U: Into<Connection>,
    V: Into<Connection>,
{
    fn from(bundle: (T, Option<U>, V)) -> Self {
        let (c1, c2, c3) = bundle;
        Self {
            sig: c1.into(),
            clk: c2.map(|c| c.into()),
            sync: Some(c3.into()),
        }
    }
}

impl<T, U, V> From<(T, U, Option<V>)> for SerialConnections
where
    T: Into<Connection>,
    U: Into<Connection>,
    V: Into<Connection>,
{
    fn from(bundle: (T, U, Option<V>)) -> Self {
        let (c1, c2, c3) = bundle;
        Self {
            sig: c1.into(),
            clk: Some(c2.into()),
            sync: c3.map(|c| c.into()),
        }
    }
}

impl<T, U, V> From<(T, Option<U>, Option<V>)> for SerialConnections
where
    T: Into<Connection>,
    U: Into<Connection>,
    V: Into<Connection>,
{
    fn from(bundle: (T, Option<U>, Option<V>)) -> Self {
        let (c1, c2, c3) = bundle;
        Self {
            sig: c1.into(),
            clk: c2.map(|c| c.into()),
            sync: c3.map(|c| c.into()),
        }
    }
}

/// High-level description of a particular sequencing event.
///
/// Every `Event` is checked upon creation to guarantee that the associated
/// [`Connection`] object agrees with the kind of `Event` being created:
///
/// | `Event` kind            | `Connection` type |
/// |:------------------------|:------------------|
/// | [`Event::digital`]      | `digital`         |
/// | [`Event::digital_data`] | `digital`         |
/// | [`Event::pulse`]        | `digital`         |
/// | [`Event::serial`]       | `digital`         |
/// | [`Event::serial_ramp`]  | `digital`         |
/// | [`Event::serial_data`]  | `digital`         |
/// | [`Event::analog`]       | `analog`          |
/// | [`Event::analog_ramp`]  | `analog`          |
/// | [`Event::analog_data`]  | `analog`          |
///
/// Of these, `Event::digital` and `Event::analog` are considered "singles",
/// meaning that they refer to a digital or analog state on only a single
/// channel at a single time. All `new_*` constructor functions take a parameter
/// `with_delay` that controls whether the timing of the event is ultimately
/// adjusted to compensate for the delay held in the `Connection`.
///
/// Additionally, every `Event` can be "compiled" down to a [`Sequence`] of
/// only [singles][`Event::to_singles`] (useful for visualization) or a
/// `Sequence` of [`State`]s, which contain only the primitives that are
/// eventually sent to a [`Computer`][crate::structures::computer::Computer]
/// for ultimate scheduling.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Event {
    pub(crate) data: EventData,
}

impl Event {
    /// Create a new digital event.
    ///
    /// This event is *single*.
    pub fn new_digital<V>(
        conn: Connection,
        state: V,
        time: f64,
        with_delay: bool,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        conn.is_digital().then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        let state: DigitalState
            = state.for_conn_type(ConnectionType::Digital).into();
        Ok(
            Self {
                data: EventData::Digital {
                    conn,
                    state,
                    time,
                    with_delay,
                }
            }
        )
    }

    /// Create a new digital event, accounting for channel delays.
    ///
    /// This event is *single*.
    pub fn digital<V>(
        conn: Connection,
        state: V,
        time: f64,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_digital(conn, state, time, true)
    }

    /// Create a new digital event, without accounting for channel delays.
    ///
    /// This event is *single*.
    pub fn digital_nodelay<V>(
        conn: Connection,
        state: V,
        time: f64,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_digital(conn, state, time, false)
    }

    /// Create a new digital data series event. Series must be non-empty.
    ///
    /// This event is *not single*.
    pub fn new_digital_data<I, V>(
        conn: Connection,
        data: I,
        with_delay: bool,
    ) -> EWResult<Self>
    where
        I: IntoIterator<Item = (f64, V)>,
        V: ToVoltage,
    {
        conn.is_digital().then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        let data: Vec<(f64, DigitalState)>
            = data.into_iter()
            .map(|(t, v)| (t, v.for_conn_type(ConnectionType::Digital).into()))
            .collect();
        (!data.is_empty()).then_some(())
            .ok_or(EWError::EmptyTimeSeries)?;
        Ok(
            Self {
                data: EventData::DigitalData {
                    conn,
                    data,
                    with_delay,
                }
            }
        )
    }

    /// Create a new digital data series event, accounting for channel delays.
    /// Series must be non-empty.
    ///
    /// This event is *not single*.
    pub fn digital_data<I, V>(
        conn: Connection,
        data: I,
    ) -> EWResult<Self>
    where
        I: IntoIterator<Item = (f64, V)>,
        V: ToVoltage,
    {
        Self::new_digital_data(conn, data, true)
    }

    /// Create a new digital data series event, not accounting for channel
    /// delays.
    ///
    /// This event is *not single*.
    pub fn digital_data_nodelay<I, V>(
        conn: Connection,
        data: I,
    ) -> EWResult<Self>
    where
        I: IntoIterator<Item = (f64, V)>,
        V: ToVoltage,
    {
        Self::new_digital_data(conn, data, false)
    }

    /// Create a new digital pulse event.
    ///
    /// This event is *not single*.
    pub fn new_pulse<V>(
        conn: Connection,
        level: V,
        time: Range<f64>,
        with_delay: bool,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        conn.is_digital().then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        let level: DigitalState
            = level.for_conn_type(ConnectionType::Digital).into();
        let inverted: bool = !bool::from(level);
        let Range { start: time_beg, end: time_end } = time;
        (time_end > time_beg).then_some(())
            .ok_or(EWError::InvalidEventTimes)?;
        Ok(
            Self {
                data: EventData::Pulse {
                    conn,
                    inverted,
                    time_beg,
                    time_end,
                    with_delay,
                }
            }
        )
    }

    /// Create a new digital pulse event, accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn pulse<V>(
        conn: Connection,
        level: V,
        time: Range<f64>,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_pulse(conn, level, time, true)
    }

    /// Create a new digital pulse event, not accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn pulse_nodelay<V>(
        conn: Connection,
        level: V,
        time: Range<f64>,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_pulse(conn, level, time, false)
    }

    /// Create a new digital serial event.
    ///
    /// This event is *not single*.
    pub fn new_serial<C, M, R>(
        conns: C,
        msg: M,
        reg: R,
        time: f64,
        with_delay: bool,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        let SerialConnections { sig, clk, sync } = conns.into();
        (
            sig.is_digital()
            || clk.map(|c| c.is_digital()).unwrap_or(false)
            || sync.map(|c| c.is_digital()).unwrap_or(false)
        ).then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        let SerialMessage { val, len } = msg.into();
        let SerialMessage { val: reg, len: len_reg } = reg.into();
        Ok(
            Self {
                data: EventData::Serial {
                    conn: sig,
                    conn_clk: clk,
                    conn_sync: sync,
                    val,
                    len,
                    reg,
                    len_reg,
                    time,
                    with_delay,
                }
            }
        )
    }

    /// Create a new digital serial event, accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn serial<C, M, R>(
        conns: C,
        msg: M,
        reg: R,
        time: f64,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        Self::new_serial(conns, msg, reg, time, true)
    }

    /// Create a new digital serial event, not accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn serial_nodelay<C, M, R>(
        conns: C,
        msg: M,
        reg: R,
        time: f64,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        Self::new_serial(conns, msg, reg, time, false)
    }

    /// Create a new digital serial ramp event.
    ///
    /// This event is *not single*.
    pub fn new_serial_ramp<C, M, R>(
        conns: C,
        msg_range: Range<M>,
        reg: R,
        time: Range<f64>,
        n_steps: usize,
        with_delay: bool,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        let SerialConnections { sig, clk, sync } = conns.into();
        (
            sig.is_digital()
            || clk.map(|c| c.is_digital()).unwrap_or(false)
            || sync.map(|c| c.is_digital()).unwrap_or(false)
        ).then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        let Range { start: m0, end: m1 } = msg_range;
        let SerialMessage { val: val_beg, len: len_beg } = m0.into();
        let SerialMessage { val: val_end, len: len_end } = m1.into();
        let SerialMessage { val: reg, len: len_reg } = reg.into();
        let Range { start: time_beg, end: time_end } = time;
        (time_end > time_beg).then_some(())
            .ok_or(EWError::InvalidEventTimes)?;
        Ok(
            Self {
                data: EventData::SerialRamp {
                    conn: sig,
                    conn_clk: clk,
                    conn_sync: sync,
                    val_beg,
                    val_end,
                    len: len_beg.max(len_end),
                    reg,
                    len_reg,
                    time_beg,
                    time_end,
                    n_steps,
                    with_delay,
                }
            }
        )
    }

    /// Create a new digital serial ramp event, accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn serial_ramp<C, M, R>(
        conns: C,
        msg_range: Range<M>,
        reg: R,
        time: Range<f64>,
        n_steps: usize,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        Self::new_serial_ramp(
            conns, msg_range, reg, time, n_steps, true)
    }

    /// Create a new digital serial ramp event, not accounting for channel
    /// delays.
    ///
    /// This event is *not single*.
    pub fn serial_ramp_nodelay<C, M, R>(
        conns: C,
        msg_range: Range<M>,
        reg: R,
        time: Range<f64>,
        n_steps: usize,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        Self::new_serial_ramp(
            conns, msg_range, reg, time, n_steps, false)
    }

    /// Create a new digital serial data series event. Series must be non-empty.
    ///
    /// This event is *not single*.
    pub fn new_serial_data<C, I, M, R>(
        conns: C,
        data: I,
        reg: R,
        with_delay: bool,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        I: IntoIterator<Item = (f64, M)>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        let SerialConnections { sig, clk, sync } = conns.into();
        (
            sig.is_digital()
            || clk.map(|c| c.is_digital()).unwrap_or(false)
            || sync.map(|c| c.is_digital()).unwrap_or(false)
        ).then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        let data: Vec<(f64, SerialMessage)>
            = data.into_iter()
            .map(|(t, m)| (t, m.into()))
            .collect();
        let len: u8
            = data.iter()
            .map(|(_, msg)| msg.len)
            .max()
            .ok_or(EWError::EmptyTimeSeries)?;
        let data: Vec<(f64, u32)>
            = data.into_iter()
            .map(|(t, msg)| (t, msg.val))
            .collect();
        let SerialMessage { val: reg, len: len_reg } = reg.into();
        Ok(
            Self {
                data: EventData::SerialData {
                    conn: sig,
                    conn_clk: clk,
                    conn_sync: sync,
                    data,
                    len,
                    reg,
                    len_reg,
                    with_delay,
                }
            }
        )
    }

    /// Create a new digital serial data series event, accounting for channel
    /// delays. Series must be non-empty.
    ///
    /// This event is *not single*.
    pub fn serial_data<C, I, M, R>(
        conns: C,
        data: I,
        reg: R,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        I: IntoIterator<Item = (f64, M)>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        Self::new_serial_data(conns, data, reg, true)
    }

    /// Create a new digital serial data series event, not accounting for
    /// channel delays. Series must be non-empty.
    ///
    /// This event is *not single*.
    pub fn serial_data_nodelay<C, I, M, R>(
        conns: C,
        data: I,
        reg: R,
    ) -> EWResult<Self>
    where
        C: Into<SerialConnections>,
        I: IntoIterator<Item = (f64, M)>,
        M: Into<SerialMessage>,
        R: Into<SerialMessage>,
    {
        Self::new_serial_data(conns, data, reg, false)
    }

    /// Create a new analog event.
    ///
    /// This event is *single*.
    pub fn new_analog<V>(
        conn: Connection,
        val: V,
        time: f64,
        with_delay: bool,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        conn.is_analog().then_some(())
            .ok_or(EWError::InvalidConnectionAnalog)?;
        let val: f64 = val.for_conn_type(ConnectionType::Analog).into();
        Ok(
            Self {
                data: EventData::Analog {
                    conn,
                    val,
                    time,
                    with_delay,
                }
            }
        )
    }

    /// Create a new analog event, accounting for channel delays.
    ///
    /// This event is *single*.
    pub fn analog<V>(
        conn: Connection,
        val: V,
        time: f64,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_analog(conn, val, time, true)
    }

    /// Create a new analog event, not accounting for channel delays.
    ///
    /// This event is *single*.
    pub fn analog_nodelay<V>(
        conn: Connection,
        val: V,
        time: f64,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_analog(conn, val, time, false)
    }

    /// Create a new analog ramp event.
    ///
    /// This event is *not single*.
    pub fn new_analog_ramp<V>(
        conn: Connection,
        val_range: Range<V>,
        time: Range<f64>,
        n_steps: usize,
        with_delay: bool,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        conn.is_analog().then_some(())
            .ok_or(EWError::InvalidConnectionAnalog)?;
        let Range { start: val_beg, end: val_end } = val_range;
        let val_beg: f64
            = val_beg.for_conn_type(ConnectionType::Analog).into();
        let val_end: f64
            = val_end.for_conn_type(ConnectionType::Analog).into();
        let Range { start: time_beg, end: time_end } = time;
        (time_end > time_beg).then_some(())
            .ok_or(EWError::InvalidEventTimes)?;
        Ok(
            Self {
                data: EventData::AnalogRamp {
                    conn,
                    val_beg,
                    val_end,
                    time_beg,
                    time_end,
                    n_steps,
                    with_delay,
                }
            }
        )
    }

    /// Create a new analog ramp event, accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn analog_ramp<V>(
        conn: Connection,
        val_range: Range<V>,
        time: Range<f64>,
        n_steps: usize,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_analog_ramp(conn, val_range, time, n_steps, true)
    }

    /// Create a new analog ramp event, not accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn analog_ramp_nodelay<V>(
        conn: Connection,
        val_range: Range<V>,
        time: Range<f64>,
        n_steps: usize,
    ) -> EWResult<Self>
    where V: ToVoltage
    {
        Self::new_analog_ramp(conn, val_range, time, n_steps, false)
    }

    /// Create a new analog data series event. Series must be non-empty.
    ///
    /// This event is *not single*.
    pub fn new_analog_data<I, V>(
        conn: Connection,
        data: I,
        with_delay: bool,
    ) -> EWResult<Self>
    where
        I: IntoIterator<Item = (f64, V)>,
        V: ToVoltage,
    {
        conn.is_analog().then_some(())
            .ok_or(EWError::InvalidConnectionAnalog)?;
        let data: Vec<(f64, f64)>
            = data.into_iter()
            .map(|(t, v)| (t, v.for_conn_type(ConnectionType::Analog).into()))
            .collect();
        (!data.is_empty()).then_some(())
            .ok_or(EWError::EmptyTimeSeries)?;
        Ok(
            Self {
                data: EventData::AnalogData {
                    conn,
                    data,
                    with_delay,
                }
            }
        )
    }

    /// Create a new analog data series event, accounting for channel delays.
    ///
    /// This event is *not single*.
    pub fn analog_data<I, V>(
        conn: Connection,
        data: I,
    ) -> EWResult<Self>
    where
        I: IntoIterator<Item = (f64, V)>,
        V: ToVoltage,
    {
        Self::new_analog_data(conn, data, true)
    }

    /// Create a new analog data series event, not accounting for channel
    /// delays.
    ///
    /// This event is *not single*.
    pub fn analog_data_nodelay<I, V>(
        conn: Connection,
        data: I,
    ) -> EWResult<Self>
    where
        I: IntoIterator<Item = (f64, V)>,
        V: ToVoltage,
    {
        Self::new_analog_data(conn, data, false)
    }

    /// Return `true` if `self` is a single digital event, `false` otherwise.
    pub fn is_digital(&self) -> bool {
        matches!(self.data, EventData::Digital { .. })
    }

    /// Returns `true` if `self` is a digital data event, `false` otherwise.
    pub fn is_digital_data(&self) -> bool {
        matches!(self.data, EventData::Digital { .. })
    }

    /// Return `true` if `self` is a digital pulse event, `false` otherwise.
    pub fn is_pulse(&self) -> bool {
        matches!(self.data, EventData::Pulse { .. })
    }

    /// Return `true` if `self` is a digital serial event, `false` otherwise.
    pub fn is_serial(&self) -> bool {
        matches!(self.data, EventData::Serial { .. })
    }

    /// Return `true` if `self` is a digital serial ramp event, `false`
    /// otherwise.
    pub fn is_serial_ramp(&self) -> bool {
        matches!(self.data, EventData::SerialRamp { .. })
    }

    /// Return `true` if `self` is a digital serial data event, `false`
    /// otherwise.
    pub fn is_serial_data(&self) -> bool {
        matches!(self.data, EventData::SerialData { .. })
    }

    /// Return `true` if `self` is a single analog event, `false` otherwise.
    pub fn is_analog(&self) -> bool {
        matches!(self.data, EventData::Analog { .. })
    }

    /// Return `true` if `self` is an analog ramp event, `false` otherwise.
    pub fn is_analog_ramp(&self) -> bool {
        matches!(self.data, EventData::AnalogRamp { .. })
    }

    /// Return `true` if `self` is an analog data event, `false` otherwise.
    pub fn is_analog_data(&self) -> bool {
        matches!(self.data, EventData::AnalogData { .. })
    }

    /// Return `true` if `self` is any digital event, `false` otherwise.
    pub fn is_any_digital(&self) -> bool {
        self.is_digital()
            || self.is_pulse()
            || self.is_serial()
            || self.is_serial_ramp()
    }

    /// Return `true` if `self` is any analog event, `false` otherwise.
    pub fn is_any_analog(&self) -> bool {
        self.is_analog()
            || self.is_analog_ramp()
    }

    /// Return `true` if `self` is any single event, `false` otherwise.
    pub fn is_single(&self) -> bool { self.is_digital() || self.is_analog() }

    /// Return the connection of `self`, if `self` is single.
    pub fn get_connection_single(&self) -> Option<Connection> {
        match self.data {
            EventData::Digital { conn, .. } => Some(conn),
            EventData::Analog { conn, .. } => Some(conn),
            _ => None,
        }
    }

    /// Return whether `self` is single and its connection matches the given
    /// one.
    pub fn is_connection_single(&self, conn: &Connection) -> Option<bool> {
        self.get_connection_single().map(|c| &c == conn)
    }

    /// Return the voltage described by `self` if `self` is single.
    pub fn get_voltage_single(&self) -> Option<Voltage> {
        match self.data {
            EventData::Digital { conn, state, .. }
                => Some(state.for_conn(&conn)),
            EventData::Analog { conn, val, .. }
                => Some(val.for_conn(&conn)),
            _ => None,
        }
    }

    /// Return the time at which `self` occurs if `self` is single.
    pub fn get_time_single(&self) -> Option<f64> {
        match self.data {
            EventData::Digital { time, .. } => Some(time),
            EventData::Analog { time, .. } => Some(time),
            _ => None,
        }
    }

    /// Return the time and voltage of `self` if `self` is single.
    pub fn get_point_single(&self) -> Option<(f64, Voltage)> {
        self.get_time_single()
            .and_then(|t| self.get_voltage_single().map(|v| (t, v)))
    }

    /// Return the time and voltage of `self` if `self` is single.
    ///
    /// Time is returned as a `(f64, f64)` giving the real (when the channel's
    /// state is switched) and delayed (after any estimated delays have
    /// occurred) times of the actual event.
    #[allow(clippy::collapsible_else_if)]
    pub fn get_point_single_delay(&self) -> Option<((f64, f64), Voltage)> {
        match self.data {
            EventData::Digital { conn, state, time, with_delay } => {
                let (del_up, del_dn): (f64, f64) = conn.get_delay_d().unwrap();
                let (t0, t1): (f64, f64)
                    = if with_delay {
                        if state.into() {
                            (time - del_up, time)
                        } else {
                            (time - del_dn, time)
                        }
                    } else {
                        if state.into() {
                            (time, time + del_up)
                        } else {
                            (time, time + del_dn)
                        }
                    };
                Some(((t0, t1), state.for_conn(&conn)))
            },
            EventData::Analog { conn, val, time, with_delay } => {
                let del: f64 = conn.get_delay_a().unwrap();
                let (t0, t1): (f64, f64)
                    = if with_delay {
                        (time - del, time)
                    } else {
                        (time, time + del)
                    };
                Some(((t0, t1), val.for_conn(&conn)))
            },
            _ => None,
        }
    }

    /// Compile `self` to a [`Sequence`] of only single events.
    ///
    /// Events that are already single are converted to a `Sequence` of length
    /// 1.
    pub fn into_singles(self) -> Sequence<Event> {
        match self.data {
            EventData::Digital { .. } => {
                vec![self].into()
            },

            EventData::DigitalData {
                conn,
                data,
                with_delay,
            } => {
                data.into_iter()
                    .map(|(t, s)| Self::new_digital(conn, s, t, with_delay))
                    .collect::<EWResult<Vec<Event>>>()
                    .unwrap()
                    .into()
            },

            EventData::Pulse {
                conn,
                inverted,
                time_beg,
                time_end,
                with_delay,
            } => {
                vec![
                    Self::new_digital(
                        conn,
                        if inverted {
                            DigitalState::LOW
                        } else {
                            DigitalState::HIGH
                        },
                        time_beg,
                        with_delay,
                    ).unwrap(),
                    Self::new_digital(
                        conn,
                        if inverted {
                            DigitalState::HIGH
                        } else {
                            DigitalState::LOW
                        },
                        time_end,
                        with_delay,
                    ).unwrap(),
                ].into()
            },

            EventData::Serial {
                conn,
                conn_clk,
                conn_sync,
                val,
                len,
                reg,
                len_reg,
                time,
                with_delay,
            } => {
                let mut bits_reg: Vec<u8> = to_bits(reg, len_reg as usize);
                bits_reg.reverse();
                let mut bits_val: Vec<u8> = to_bits(val, len as usize);
                bits_val.reverse();
                let bits: Vec<u8> = [bits_reg, bits_val].concat();
                let T: f64 = 1.0 / SERIAL_CLOCK_FREQ;
                let mut seq: Sequence<Event> = Sequence::default();
                if let Some(clk) = conn_clk {
                    for (k, b) in bits.iter().enumerate() {
                        seq <<=
                            Event::new_digital(
                                conn, *b, time + k as f64 * T - T / 2.0,
                                with_delay,
                            ).unwrap()
                            << Event::new_digital(
                                conn, false, time + k as f64 * T + T / 4.0,
                                with_delay,
                            ).unwrap()
                            << Event::new_digital(
                                clk, true, time + k as f64 * T - T / 2.0,
                                with_delay,
                            ).unwrap()
                            << Event::new_digital(
                                clk, false, time + k as f64 * T,
                                with_delay,
                            ).unwrap();
                    }
                } else {
                    for (k, b) in bits.iter().enumerate() {
                        seq <<=
                            Event::new_digital(
                                conn, *b, time + k as f64 * T,
                                with_delay,
                            ).unwrap()
                            << Event::new_digital(
                                conn, false, time + k as f64 * T + T / 2.0,
                                with_delay,
                            ).unwrap();
                    }
                }
                if let Some(sync) = conn_sync {
                    let clk: f64 = if conn_clk.is_some() { 1.0 } else { 0.0 };
                    seq <<=
                        Event::new_digital(
                            sync, true, time - T / 2.0 - clk * T / 2.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            sync, false, time - clk * T / 2.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            sync, true, time + bits.len() as f64 * T - clk * T / 2.0,
                            with_delay,
                        ).unwrap();
                }
                seq
            },

            EventData::SerialRamp {
                conn,
                conn_clk,
                conn_sync,
                val_beg,
                val_end,
                len,
                reg,
                len_reg,
                time_beg,
                time_end,
                n_steps,
                with_delay,
            } => {
                let dt: f64 = (time_end - time_beg) / n_steps as f64;
                let val_beg: f64 = val_beg as f64;
                let val_end: f64 = val_end as f64;
                let dv: f64 = (val_end - val_beg) / n_steps as f64;
                (0..n_steps + 1)
                    .flat_map(|k| {
                        Self::new_serial(
                            (conn, conn_clk, conn_sync),
                            ((val_beg + dv * k as f64) as u32, len),
                            (reg, len_reg),
                            time_beg + dt * k as f64,
                            with_delay,
                        )
                        .unwrap()
                        .into_singles()
                    })
                    .collect()
            },

            EventData::SerialData {
                conn,
                conn_clk,
                conn_sync,
                data,
                len,
                reg,
                len_reg,
                with_delay,
            } => {
                data.into_iter()
                    .flat_map(|(t, u)| {
                        Self::new_serial(
                            (conn, conn_clk, conn_sync),
                            (u, len),
                            (reg, len_reg),
                            t,
                            with_delay,
                        )
                        .unwrap()
                        .into_singles()
                    })
                    .collect()
            },

            EventData::Analog { .. } => {
                vec![self].into()
            },

            EventData::AnalogRamp {
                conn,
                val_beg,
                val_end,
                time_beg,
                time_end,
                n_steps,
                with_delay,
            } => {
                let dt: f64 = (time_end - time_beg) / n_steps as f64;
                let dv: f64 = (val_end - val_beg) / n_steps as f64;
                (0..n_steps + 1)
                    .map(|k| {
                        Self::new_analog(
                            conn,
                            val_beg + dv * k as f64,
                            time_beg + dt * k as f64,
                            with_delay,
                        )
                        .unwrap()
                    })
                    .collect()
            },

            EventData::AnalogData {
                conn,
                data,
                with_delay,
            } => {
                data.into_iter()
                    .map(|(t, v)| Self::new_analog(conn, v, t, with_delay))
                    .collect::<EWResult<Vec<Event>>>()
                    .unwrap()
                    .into()
            },
        }
    }

    /// Compile `self` to a [`Sequence`] of only single events.
    ///
    /// Events that are already single are converted to a `Sequence` of length
    /// 1.
    pub fn to_singles(&self) -> Sequence<Event> { self.clone().into_singles() }

    /// Compile `self` to a [`Sequence`] of only single events for viewing
    /// purposes, discarding details where possible.
    ///
    /// Events that are already single are converted to a `Sequence` of length
    /// 1.
    pub fn into_singles_viewer(self) -> Sequence<Event> {
        match self.data {
            EventData::Digital { .. } => self.into_singles(),
            EventData::DigitalData { .. } => self.into_singles(),
            EventData::Pulse { .. } => self.into_singles(),
            EventData::Serial {
                conn,
                conn_clk,
                conn_sync,
                val: _,
                len,
                reg: _,
                len_reg,
                time,
                with_delay,
            } => {
                let T: f64 = 1.0 / SERIAL_CLOCK_FREQ;
                let last_bit: f64 = (len + len_reg) as f64 - 1.0;
                let mut seq: Sequence<Event> = Sequence::default();
                if let Some(clk) = conn_clk {
                    seq
                        <<= Event::new_digital(
                            conn, 1, time - T / 2.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            conn, 0, time + last_bit * T + T / 4.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            clk, 1, time - T / 2.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            clk, 0, time + last_bit * T,
                            with_delay,
                        ).unwrap();
                } else {
                    seq
                        <<= Event::new_digital(
                            conn, 1, time,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            conn, 0, time + last_bit * T + T / 2.0,
                            with_delay,
                        ).unwrap();
                }
                if let Some(sync) = conn_sync {
                    let clk: f64 = if conn_clk.is_some() { 1.0 } else { 0.0 };
                    seq
                        <<= Event::new_digital(
                            sync, 1, time - T / 2.0 - clk * T / 2.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            sync, 0, time - clk * T / 2.0,
                            with_delay,
                        ).unwrap()
                        << Event::new_digital(
                            sync, 1, time + last_bit * T - clk * T / 2.0,
                            with_delay,
                        ).unwrap();
                }
                seq
            },

            EventData::SerialRamp {
                conn,
                conn_clk,
                conn_sync,
                val_beg,
                val_end,
                len,
                reg,
                len_reg,
                time_beg,
                time_end,
                n_steps,
                with_delay,
            } => {
                let dt: f64 = (time_end - time_beg) / n_steps as f64;
                let val_beg: f64 = val_beg as f64;
                let val_end: f64 = val_end as f64;
                let dv: f64 = (val_end - val_beg) / n_steps as f64;
                (0..n_steps + 1)
                    .flat_map(|k| {
                        Self::new_serial(
                            (conn, conn_clk, conn_sync),
                            ((val_beg + dv * k as f64) as u32, len),
                            (reg, len_reg),
                            time_beg + dt * k as f64,
                            with_delay,
                        )
                        .unwrap()
                        .into_singles_viewer()
                    })
                    .collect()
            },

            EventData::SerialData {
                conn,
                conn_clk,
                conn_sync,
                data,
                len,
                reg,
                len_reg,
                with_delay,
            } => {
                data.into_iter()
                    .flat_map(|(t, u)| {
                        Self::new_serial(
                            (conn, conn_clk, conn_sync),
                            (u, len),
                            (reg, len_reg),
                            t,
                            with_delay,
                        )
                        .unwrap()
                        .into_singles_viewer()
                    })
                    .collect()
            },
            EventData::Analog { .. } => self.into_singles(),
            EventData::AnalogRamp { .. } => self.into_singles(),
            EventData::AnalogData { .. } => self.into_singles(),
        }
    }

    /// Compile `self` to a [`Sequence`] of only single events for viewing
    /// purposes, discarding details where possible.
    ///
    /// Events that are already single are converted to a `Sequence` of length
    /// 1.
    pub fn to_singles_viewer(&self) -> Sequence<Event> {
        self.clone().into_singles_viewer()
    }

    /// Compile `self` to a [`Sequence`] of [`State`]s.
    pub fn into_states(self) -> EWResult<Sequence<State>> {
        match self.data {
            EventData::Digital {
                conn,
                state,
                time,
                with_delay,
            } => {
                let seq: Sequence<State>
                    = vec![State::new(conn, state, time, with_delay)?].into();
                Ok(seq)
            },

            EventData::Analog {
                conn,
                val,
                time,
                with_delay,
            } => {
                let seq: Sequence<State>
                    = vec![State::new(conn, val, time, with_delay)?].into();
                Ok(seq)
            },

            EventData::DigitalData { .. }
            | EventData::Pulse { .. }
            | EventData::Serial { .. }
            | EventData::SerialRamp { .. }
            | EventData::SerialData { .. }
            | EventData::AnalogRamp { .. }
            | EventData::AnalogData { .. }
            => {
                let mut acc: Vec<State> = Vec::new();
                for e in self.into_singles().into_iter() {
                    acc.append(e.into_states()?.as_mut());
                }
                Ok(acc.into())
            },
        }
    }

    /// Compile `self` to a [`Sequence`] of [`State`]s.
    pub fn to_states(&self) -> EWResult<Sequence<State>> {
        self.clone().into_states()
    }
}

impl Timed for Event {
    fn min_time(&self) -> f64 {
        match &self.data {
            EventData::Digital { time, .. } => *time,
            EventData::DigitalData { data, .. } => {
                data.iter()
                    .map(|(t, _)| t)
                    .min_by(|l, r| l.total_cmp(r))
                    .copied()
                    .unwrap()
            },
            EventData::Pulse { time_beg, .. } => *time_beg,
            EventData::Serial { time, .. } => *time,
            EventData::SerialRamp { time_beg, .. } => *time_beg,
            EventData::SerialData { data, .. } => {
                data.iter()
                    .map(|(t, _)| t)
                    .min_by(|l, r| l.total_cmp(r))
                    .copied()
                    .unwrap()
            },
            EventData::Analog { time, .. } => *time,
            EventData::AnalogRamp { time_beg, .. } => *time_beg,
            EventData::AnalogData { data, .. } => {
                data.iter()
                    .map(|(t, _)| t)
                    .min_by(|l, r| l.total_cmp(r))
                    .copied()
                    .unwrap()
            },
        }
    }

    fn max_time(&self) -> f64 {
        match &self.data {
            EventData::Digital { time, .. } => *time,
            EventData::DigitalData { data, .. } => {
                data.iter()
                    .map(|(t, _)| t)
                    .max_by(|l, r| l.total_cmp(r))
                    .copied()
                    .unwrap()
            },
            EventData::Pulse { time_end, .. } => *time_end,
            EventData::Serial { time, .. } => *time,
            EventData::SerialRamp { time_end, .. } => *time_end,
            EventData::SerialData { data, .. } => {
                data.iter()
                    .map(|(t, _)| t)
                    .max_by(|l, r| l.total_cmp(r))
                    .copied()
                    .unwrap()
            },
            EventData::Analog { time, .. } => *time,
            EventData::AnalogRamp { time_end, .. } => *time_end,
            EventData::AnalogData { data, .. } => {
                data.iter()
                    .map(|(t, _)| t)
                    .max_by(|l, r| l.total_cmp(r))
                    .copied()
                    .unwrap()
            },
        }
    }
}


