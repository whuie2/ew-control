//! Provides [`Computer`], the main interface to sequenced and real-time
//! control of the Entangleware backend.

use std::{
    net::SocketAddr,
};
use thiserror::Error;
use crate::{
    structures::{
        EWError,
        EWResult,
        connection::{
            Connection,
            ConnectionLayout,
        },
        sequence::Sequence,
        state::State,
        voltage::{
            ToVoltage,
            Voltage,
        },
    },
    entangleware_control_link::{
        ControlLink,
        CHANNELS_D,
    },
};

#[derive(Debug, Error)]
pub enum ComputerError {
    #[error("defaults must be set before queueing")]
    DefaultWhileQueueing,

    #[error("connection key '{0}' not found")]
    UndefinedConnectionKey(String),
}

/// Main interface for connecting to and communicating with the backend.
///
/// Each `Computer` stores a [`ConnectionLayout`] that defines a set of named
/// connections accessible through [`Computer::c`] with default states that can
/// be reverted to with [`Computer::reset_defaults`].
pub struct Computer {
    control_link: ControlLink,
    defaults: ConnectionLayout,
    original_defaults: ConnectionLayout,
}

impl Computer {
    /// Open a connection to Entangleware Control.
    ///
    /// Only a single such connection can be open at a time.
    pub fn connect<A>(
        addr: A,
        ttl: Option<u32>,
        timeout_sec: Option<f64>,
        defaults: ConnectionLayout,
    ) -> EWResult<Self>
    where A: Into<SocketAddr>,
    {
        Ok(
            Self {
                control_link: ControlLink::connect(timeout_sec, addr, ttl)?,
                defaults: defaults.clone(),
                original_defaults: defaults,
            }
        )
    }

    /// Disconnect from Entangleware Control, consuming `self`.
    pub fn disconnect(self) { }

    /// Get a reference to the internal connection layout.
    pub fn get_connections(&self) -> &ConnectionLayout {
        &self.defaults
    }

    /// Get a mutable reference to the internal connection layout.
    pub fn get_connections_mut(&mut self) -> &mut ConnectionLayout {
        &mut self.defaults
    }

    /// Get a reference to the original connection layout from when `self` was
    /// instantiated.
    pub fn get_connections_orig(&self) -> &ConnectionLayout {
        &self.original_defaults
    }

    /// Get a mutable reference to the original connection layout from when
    /// `self` was instantiated.
    pub fn get_connections_orig_mut(&mut self) -> &mut ConnectionLayout {
        &mut self.original_defaults
    }

    /// Get a named connection from the layout.
    pub fn get_connection(&self, key: &str) -> Option<Connection> {
        self.defaults.get(key).copied()
    }

    /// Get a mutable reference to a named connection from the layout.
    pub fn get_connection_mut(&mut self, key: &str)
        -> Option<&mut Connection>
    {
        self.defaults.get_mut(key)
    }

    /// Like [`Self::get_connection`], but with a shorter name and returning a
    /// `Result` so that `?` can be used for brevity.
    pub fn c(&self, key: &str) -> EWResult<Connection> {
        Ok(
            self.get_connection(key)
            .ok_or(ComputerError::UndefinedConnectionKey(key.to_string()))?
        )
    }

    /// Show (or not) the Entangleware debug window.
    pub fn set_debug_mode(&mut self, onoff: bool) -> EWResult<&mut Self> {
        self.control_link.set_debug_mode(onoff)?;
        Ok(self)
    }

    /// Return the queueing status.
    pub fn is_queueing(&self) -> bool { self.control_link.is_building() }

    /// Set the default state of a named [`Connection`].
    ///
    /// This is used for real-time control only, and cannot be done if sequences
    /// are currently being queued. Setting a default value with this method
    /// mutates the underlying `Connection` object of the layout in addition to
    /// changing the actual output voltage.
    pub fn set_default<V>(&mut self, key: &str, state: V) -> EWResult<&mut Self>
    where V: ToVoltage
    {
        const BOARD: u8 = 0;

        (!self.is_queueing()).then_some(())
            .ok_or(ComputerError::DefaultWhileQueueing)?;
        let conn = self.get_connection_mut(key)
            .ok_or(ComputerError::UndefinedConnectionKey(key.to_string()))?;

        conn.set_state(state);
        let state = State::new(*conn, state, 0.0, false)?;
        match state {
            State::Digital(connector, mask, state, _) => {
                self.control_link.set_digital_state(
                    0.0, BOARD, connector, mask, mask, state)?;
            },
            State::Analog(channel, state, _) => {
                self.control_link.set_analog_state(
                    0.0, BOARD, channel, state)?;
            },
        }
        Ok(self)
    }

    /// Set the default state of an arbitrary connection.
    ///
    /// This leaves the internal state of `self` unmmutated, even if the
    /// connection address happens to match a named connection.
    pub fn set_default_address<C, V>(&mut self, conn: C, state: V)
        -> EWResult<&mut Self>
    where
        C: Into<Connection>,
        V: ToVoltage,
    {
        const BOARD: u8 = 0;

        (!self.is_queueing()).then_some(())
            .ok_or(ComputerError::DefaultWhileQueueing)?;

        let state = State::new(conn.into(), state, 0.0, false)?;
        match state {
            State::Digital(connector, mask, state, _) => {
                self.control_link.set_digital_state(
                    0.0, BOARD, connector, mask, mask, state)?;
            },
            State::Analog(channel, state, _) => {
                self.control_link.set_analog_state(
                    0.0, BOARD, channel, state)?;
            },
        }
        Ok(self)
    }

    /// Set the default state of a digital [`Connection`].
    ///
    /// This is used for real-time control only, and cannot be done if
    /// sequences are currently being queued. Fails if `conn` is analog.
    pub fn set_default_digital<V>(&mut self, key: &str, state: V)
        -> EWResult<&mut Self>
    where V: ToVoltage
    {
        self.c(key)?.is_digital().then_some(())
            .ok_or(EWError::InvalidConnectionDigital)?;
        self.set_default(key, state)
    }

    /// Set the default state of an analog [`Connection`].
    ///
    /// This is used for real-time control only, and cannot be done if
    /// sequences are currently being queued. Fails if `conn` is digital.
    pub fn set_default_analog<V>(&mut self, key: &str, state: V)
        -> EWResult<&mut Self>
    where V: ToVoltage
    {
        self.c(key)?.is_analog().then_some(())
            .ok_or(EWError::InvalidConnectionAnalog)?;
        self.set_default(key, state)
    }

    /// Set every [`Connection`] held in the stored [`ConnectionLayout`] to
    /// their original states from when `self` was initialized.
    pub fn reset_defaults(&mut self) -> EWResult<&mut Self> {
        let defaults: Vec<(String, Voltage)>
            = self.original_defaults.iter()
            .map(|(key, conn)| (key.to_string(), conn.get_default_voltage()))
            .collect();
        for (key, voltage) in defaults.into_iter() {
            self.set_default(&key, voltage)?;
        }
        Ok(self)
    }

    /// Begin queueing [`Sequence`] data.
    ///
    /// This method can be called multiple times to queue multiple sequences.
    pub fn enqueue(&mut self, sequence: Sequence<State>)
        -> EWResult<&mut Self>
    {
        const BOARD: u8 = 0;

        if !self.is_queueing() {
            self.control_link.build_sequence();
            // self.queueing = true;
        }
        for state in sequence.into_iter() {
            match state {
                State::Digital(connector, mask, state, time) => {
                    self.control_link.set_digital_state(
                        time, BOARD, connector, mask, mask, state)?;
                },
                State::Analog(channel, state, time) => {
                    self.control_link.set_analog_state(
                        time, BOARD, channel, state)?;
                },
            }
        }
        Ok(self)
    }

    /// Write the recorded sequence data to Entangleware Control and run it.
    ///
    /// This resets the queueing status and also writes the raw binary sequence
    /// data to a file called `LastCompiledRun.dat` in the current directory.
    pub fn run(&mut self, printflag: bool) -> EWResult<&mut Self> {
        // self.queueing = false;
        self.control_link.run_sequence(printflag)?;
        self.control_link.clear_sequence();
        Ok(self)
    }

    /// Read sequence data from `LastCompiledRun.dat` in the local directory,
    /// write it to Entangleware Control, and run it.
    pub fn rerun(&mut self, printflag: bool) -> EWResult<&mut Self> {
        // self.queueing = false;
        self.control_link.rerun_last_sequence(printflag)?;
        self.control_link.clear_sequence();
        Ok(self)
    }

    /// If currently queueing, call [`Self::run`]; otherwise call
    /// [`Self::rerun`].
    pub fn rrun(&mut self, printflag: bool) -> EWResult<&mut Self> {
        if self.is_queueing() {
            self.run(printflag)
        } else {
            self.rerun(printflag)
        }
    }

    /// Halt execution of the current sequence.
    pub fn stop(&mut self) -> EWResult<&mut Self> {
        self.control_link.stop_sequence()?;
        Ok(self)
    }

    /// Clear all events from memory.
    pub fn clear(&mut self) -> EWResult<&mut Self> {
        self.control_link.clear_sequence();
        Ok(self)
    }
}

/// Convenience function that returns an integer whose bits correspond to a
/// selection of enumerated channels for use with [`State::Digital`].
///
/// Repeated channel numbers toggle the state of the channel selection. Fails
/// if a channel number greater than or equal to [`CHANNELS_D`] is encountered.
pub fn channel_int<I>(ch_nums: I) -> EWResult<u32>
where I: IntoIterator<Item = u8>
{
    let mut acc: u32 = 0;
    ch_nums.into_iter()
        .map(|k| {
            (k < CHANNELS_D)
                .then(|| {
                    acc ^= 1_u32 << k as u32;
                })
                .ok_or(super::CLError::ChannelOutOfRange(k).into())
        })
        .collect::<EWResult<Vec<()>>>()?;
    Ok(acc)
}

/// Like [`channel_int`], but in macro form.
#[macro_export]
macro_rules! ch(
    ( $( $ch_num:expr ),* $(,)? ) => {
        {
            let mut acc: u32 = 0;
            $(acc ^= 1_u32 << (ch_num as u32).min(31);)*
            acc
        }
    }
);

