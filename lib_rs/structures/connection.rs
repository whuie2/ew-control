//! Descriptions for specific output ports from the computer and named
//! groupings thereof.
//!
//! [`Connection`]s can be stored under names in a [`ConnectionLayout`], which
//! is then passed to a [`Computer`][crate::structures::computer::Computer].

use std::{
    fmt,
    hash::{
        Hash,
        Hasher,
    },
    ops::{
        Deref,
        DerefMut,
        Index,
    },
};
use indexmap::IndexMap;
use serde::{
    Serialize,
    Deserialize,
};
use crate::structures::{
    indent_block,
    EWError,
    EWResult,
    voltage::{
        DigitalState,
        ToVoltage,
        Voltage,
    },
};

/// Represents a nameable analog or digital connection.
///
/// [`Connection`]s can be either digital or analog and mirror the structure
/// of their hardware. Namely, digital `Connection`s are addressed as both a
/// connector number and a channel number, whereas analog `Connection`s are
/// addressed as just a channel number. Note that although each connection
/// also carries a default state/value and some delay information, each is
/// identified only by its address:
/// ```
/// # use ew_control::structures::connection::Connection;
/// let connection_a = Connection::Digital(0, 1, true, 0.0, 0.1);
/// let connection_b = Connection::Digital(0, 1, false, 0.1, 0.0);
/// assert_eq!(connection_a, connection_b);
/// ```
///
/// Hashing and equality testing only depends on the connector/channel values.
#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum Connection {
    Digital(
        /// connector
        u8,
        /// channel
        u8,
        /// default value
        bool,
        /// delay up
        f64,
        /// delay down
        f64,
    ),
    Analog(
        /// channel
        u8,
        /// default value
        f64,
        /// delay
        f64,
    ),
}

impl Connection {
    /// Create a new digital connection with flexible arguments for the default
    /// state.
    pub fn digital<S>(
        connector: u8,
        channel: u8,
        default: S,
        delay_up: f64,
        delay_down: f64,
    ) -> Self
    where S: Into<DigitalState>
    {
        Self::Digital(
            connector,
            channel,
            default.into().into(),
            delay_up,
            delay_down,
        )
    }

    /// Constructor like [`Self::digital`].
    pub fn analog(
        channel: u8,
        default: f64,
        delay: f64,
    ) -> Self
    {
        Self::Analog(
            channel,
            default,
            delay,
        )
    }

    /// Create a new digital connection from only an address with default
    /// (i.e. zero) default state and delays.
    pub fn digital_addr(connector: u8, channel: u8) -> Self {
        Self::Digital(connector, channel, false, 0.0, 0.0)
    }

    /// Create a new analog connection from only an address with default
    /// (i.e. zero) default state and delays.
    pub fn analog_addr(channel: u8) -> Self {
        Self::Analog(channel, 0.0, 0.0)
    }

    /// Get the connection type of `self`.
    pub fn conn_type(&self) -> ConnectionType {
        match self {
            Connection::Digital(..) => ConnectionType::Digital,
            Connection::Analog(..) => ConnectionType::Analog,
        }
    }

    /// Set the default state of `self`.
    pub fn set_state<V>(&mut self, state: V)
    where V: ToVoltage
    {
        let conn_type = self.conn_type();
        let new_state = state.for_conn_type(conn_type);
        match self {
            Self::Digital(_, _, old_state, _, _) => {
                *old_state = new_state.into();
            },
            Self::Analog(_, old_state, _) => {
                *old_state = new_state.into();
            },
        }
    }

    /// Return `true` if `self` is digital, `false` otherwise.
    pub fn is_digital(&self) -> bool { matches!(self, Self::Digital(..)) }

    /// Return `true` if `self` is analog, `false` otherwise.
    pub fn is_analog(&self) -> bool { matches!(self, Self::Analog(..)) }

    /// Get the connector number if `self` is `Digital`.
    pub fn get_connector(&self) -> Option<u8> {
        match self {
            Connection::Digital(c, _, _, _, _) => Some(*c),
            Connection::Analog(_, _, _) => None,
        }
    }

    /// Get the channel number.
    pub fn get_channel(&self) -> u8 {
        match self {
            Connection::Digital(_, ch, _, _, _) => *ch,
            Connection::Analog(ch, _, _) => *ch,
        }
    }

    /// Get a `(connector, channel)` address if `self` is `Digital`.
    pub fn get_address_d(&self) -> Option<(u8, u8)> {
        match self {
            Connection::Digital(c, ch, _, _, _) => Some((*c, *ch)),
            Connection::Analog(_, _, _) => None,
        }
    }

    /// Get a `(channel,)` address if `self` is `Analog`.
    pub fn get_address_a(&self) -> Option<(u8,)> {
        match self {
            Connection::Digital(_, _, _, _, _) => None,
            Connection::Analog(ch, _, _) => Some((*ch,)),
        }
    }

    /// Get the default digital state if `self` is `Digital`.
    pub fn get_default_d(&self) -> Option<bool> {
        match self {
            Connection::Digital(_, _, d, _, _) => Some(*d),
            Connection::Analog(_, _, _) => None,
        }
    }

    /// Get the default analog state if `self` is `Analog`.
    pub fn get_default_a(&self) -> Option<f64> {
        match self {
            Connection::Digital(_, _, _, _, _) => None,
            Connection::Analog(_, d, _) => Some(*d),
        }
    }

    /// Get the default state of `self` as a [`Voltage`].
    pub fn get_default_voltage(&self) -> Voltage {
        match self {
            Connection::Digital(_, _, v, _, _) => v.for_conn(self),
            Connection::Analog(_, v, _) => v.for_conn(self),
        }
    }

    /// Get the `(rising, falling)` delays if `self` is `Digital`.
    pub fn get_delay_d(&self) -> Option<(f64, f64)> {
        match self {
            Connection::Digital(_, _, _, del_u, del_d)
                => Some((*del_u, *del_d)),
            Connection::Analog(_, _, _)
                => None,
        }
    }

    /// Get the channel delay if `self` is `Analog`.
    pub fn get_delay_a(&self) -> Option<f64> {
        match self {
            Connection::Digital(_, _, _, _, _) => None,
            Connection::Analog(_, _, del) => Some(*del),
        }
    }
}

/// Define a digital connection address.
impl From<(u8, u8)> for Connection {
    fn from(addr: (u8, u8)) -> Self { Self::digital_addr(addr.0, addr.1) }
}

/// Define an analog connection address.
impl From<(u8,)> for Connection {
    fn from(addr: (u8,)) -> Self { Self::analog_addr(addr.0) }
}

/// Define an analog connection address.
impl From<u8> for Connection {
    fn from(addr: u8) -> Self { Self::analog_addr(addr) }
}

/// The specific type of a [`Connection`].
///
/// See also [`Connection::conn_type`].
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum ConnectionType {
    Digital = 0,
    Analog = 1,
}

impl PartialEq<Connection> for Connection {
    fn eq(&self, other: &Connection) -> bool {
        match (self, other) {
            (
                Connection::Digital(cl, chl, _, _, _),
                Connection::Digital(cr, chr, _, _, _),
            ) => {
                cl == cr && chl == chr
            },
            (
                Connection::Analog(cl, _, _),
                Connection::Analog(cr, _, _),
            ) => {
                cl == cr
            },
            _ => false,
        }
    }
}

impl Eq for Connection { }

impl Hash for Connection {
    fn hash<H>(&self, state: &mut H)
    where H: Hasher
    {
        match self {
            Connection::Digital(c, ch, _, _, _) => {
                c.hash(state);
                ch.hash(state);
            },
            Connection::Analog(c, _, _) => {
                c.hash(state);
            },
        }
    }
}

impl fmt::Display for Connection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Connection::Digital(c, ch, d, del_u, del_d) => write!(f,
                "Connection::Digital(\n\
                    connector = {},\n\
                      channel = {},\n\
                      default = {},\n\
                     delay_up = {},\n\
                   delay_down = {},\n\
                )",
                c, ch, d, del_u, del_d
            ),
            Connection::Analog(ch, d, del) => write!(f,
                "Connection::Analog(\n\
                      channel = {},\n\
                      default = {},\n\
                        delay = {},\n\
                )",
                ch, d, del
            ),
        }
    }
}

pub type NamedConnection = (String, Connection);

/// Sugared [`IndexMap`] for storing a directory of named connections.
#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub struct ConnectionLayout {
    connections: IndexMap<String, Connection>,
}

impl AsRef<IndexMap<String, Connection>> for ConnectionLayout {
    fn as_ref(&self) -> &IndexMap<String, Connection> {
        &self.connections
    }
}

impl AsMut<IndexMap<String, Connection>> for ConnectionLayout {
    fn as_mut(&mut self) -> &mut IndexMap<String, Connection> {
        &mut self.connections
    }
}

impl Deref for ConnectionLayout {
    type Target = IndexMap<String, Connection>;

    fn deref(&self) -> &Self::Target { &self.connections }
}

impl DerefMut for ConnectionLayout {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.connections }
}

impl FromIterator<(String, Connection)> for ConnectionLayout {
    fn from_iter<I>(iter: I) -> ConnectionLayout
    where I: IntoIterator<Item = (String, Connection)>
    {
        Self { connections: IndexMap::from_iter(iter) }
    }
}

pub struct ConnectionLayoutIntoIter {
    iter: indexmap::map::IntoIter<String, Connection>,
}

impl Iterator for ConnectionLayoutIntoIter {
    type Item = (String, Connection);

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl IntoIterator for ConnectionLayout {
    type Item = (String, Connection);
    type IntoIter = ConnectionLayoutIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        ConnectionLayoutIntoIter { iter: self.connections.into_iter() }
    }
}

pub struct ConnectionLayoutIter<'a> {
    iter: indexmap::map::Iter<'a, String, Connection>,
}

impl<'a> Iterator for ConnectionLayoutIter<'a> {
    type Item = (&'a String, &'a Connection);

    fn next(&mut self) -> Option<Self::Item> { self.iter.next() }
}

impl<'a> IntoIterator for &'a ConnectionLayout {
    type Item = (&'a String, &'a Connection);
    type IntoIter = ConnectionLayoutIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        ConnectionLayoutIter { iter: self.connections.iter() }
    }
}

impl Index<&str> for ConnectionLayout {
    type Output = Connection;

    fn index(&self, index: &str) -> &Connection { &self.connections[index] }
}

impl Index<&str> for &ConnectionLayout {
    type Output = Connection;

    fn index(&self, index: &str) -> &Connection { &self.connections[index] }
}

impl ConnectionLayout {
    /// Create a new `ConnectionLayout`.
    pub fn new<I>(connections: I) -> Self
    where I: IntoIterator<Item = (String, Connection)>
    {
        Self { connections: connections.into_iter().collect() }
    }

    /// Copy multiple specified connections into a new `ConnectionLayout`.
    pub fn get_sublayout<'a, I>(&self, labels: I) -> EWResult<Self>
    where I: IntoIterator<Item = &'a str>
    {
        labels.into_iter()
            .map(|label| {
                self.get(label)
                    .ok_or(EWError::MissingConnectionLabel(label.to_string()))
                    .map(|c| (label.to_string(), *c))
            })
            .collect()
    }
}

/// Create a new [`ConnectionLayout`].
///
/// ```
/// # use ew_control::{
/// #     connection_layout,
/// #     structures::connection::{
/// #         Connection,
/// #         ConnectionLayout,
/// #     },
/// # };
/// let connection_a = Connection::digital_addr(0, 0);
/// let connection_b = Connection::analog_addr(0);
///
/// let layout_1 = ConnectionLayout::new([
///     ("a".to_string(), connection_a),
///     ("b".to_string(), connection_b),
/// ]);
/// let layout_2 = connection_layout!(
///     "a" => connection_a,
///     "b" => connection_b,
/// );
///
/// assert_eq!(layout_1, layout_2);
/// ```
#[macro_export]
macro_rules! connection_layout(
    ( $( $name:expr => $conn:expr ),* $(,)? ) => {
        $crate::structures::connection::ConnectionLayout::new([$(
            ( $name.to_string(), $conn )
        ),*])
    }
);

/// Shortcut to [`ConnectionLayout::get_sublayout`].
#[macro_export]
macro_rules! get_connections(
    ( $name:ident : [ $( $label:expr ),* $(,)? ] ) => {
        $name.get_sublayout([$( $label ),*])
    }
);

/// Quickly access a [`Connection`] in a [`ConnectionLayout`] and convert to a
/// [`EWResult`] if the key is missing.
#[macro_export]
macro_rules! c {
    ( $layout:ident : $key:expr ) => {
        $layout.get($key)
            .copied()
            .ok_or(EWError::MissingConnectionLabel($key.to_string()))
    }
}

impl fmt::Display for ConnectionLayout {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut connstr = String::new();
        self.connections.iter().for_each(|(label, conn)| {
            connstr.push_str(
                indent_block(&format!("{} => {}\n", label, conn), 1).as_str());
        });
        write!(f, "ConnectionLayout({{\n{}}})", connstr)
    }
}
