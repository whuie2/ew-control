//! Provides types to describe both digital and analog channel states, as well
//! as conversions between them.
//!
//! Digital states are handled by the [`DigitalState`] type, while analog
//! states are described by bare `f64`s. The [`Voltage`] type wraps both, and
//! while `Voltage` implements [`From`] to convert from `f64`s as well as
//! `bool` and integers (which are treated as digital states), [`ToVoltage`]
//! will handle the conversion in the context of a specific [`Connection`] for
//! greater flexibility.
//!
//! Once created, `Voltage`s can then be freely converted to any of these
//! primitive types.

use serde::{
    Deserialize,
    Serialize,
};
use crate::structures::connection::{
    Connection,
    ConnectionType,
};

/// Represents a voltage value that may be passed to an analog or a digital
/// channel.
///
/// Everything except f64s are converted to digital states with [`From`].
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Voltage {
    Digital(DigitalState),
    Analog(f64),
}

impl Voltage {
    /// Returns `true` if `self` is a digital voltage, `false` otherwise.
    pub fn is_digital(&self) -> bool { matches!(self, Self::Digital(_)) }

    /// Returns `true` if `self` is an analog voltage, `false` otherwise.
    pub fn is_analog(&self) -> bool { matches!(self, Self::Analog(_)) }

    /// Returns the underlying [`DigitalState`] if `self` is digital.
    pub fn get_digital(&self) -> Option<DigitalState> {
        match self {
            Self::Digital(s) => Some(*s),
            Self::Analog(_) => None,
        }
    }

    /// Returns the underlying `f64` if `self` is analog.
    pub fn get_analog(&self) -> Option<f64> {
        match self {
            Self::Digital(_) => None,
            Self::Analog(v) => Some(*v),
        }
    }
}

impl From<Voltage> for bool {
    fn from(v: Voltage) -> Self {
        match v {
            Voltage::Digital(vv) => vv.into(),
            Voltage::Analog(vv) => vv > 0.0,
        }
    }
}

impl From<bool> for Voltage {
    fn from(b: bool) -> Self { Self::Digital(b.into()) }
}

macro_rules! impl_int_from_voltage {
    ( $u:ty ) => {
        impl From<Voltage> for $u {
            fn from(v: Voltage) -> Self {
                match v {
                    Voltage::Digital(s) => s.into(),
                    Voltage::Analog(f) => f as $u,
                }
            }
        }
    }
}
impl_int_from_voltage!(u8);
impl_int_from_voltage!(u16);
impl_int_from_voltage!(u32);
impl_int_from_voltage!(u64);
impl_int_from_voltage!(u128);
impl_int_from_voltage!(usize);
impl_int_from_voltage!(i8);
impl_int_from_voltage!(i16);
impl_int_from_voltage!(i32);
impl_int_from_voltage!(i64);
impl_int_from_voltage!(i128);
impl_int_from_voltage!(isize);

macro_rules! impl_voltage_from_int {
    ( $u:ty ) => {
        impl From<$u> for Voltage {
            fn from(b: $u) -> Self { Self::Digital(b.into()) }
        }
    }
}
impl_voltage_from_int!(u8);
impl_voltage_from_int!(u16);
impl_voltage_from_int!(u32);
impl_voltage_from_int!(u64);
impl_voltage_from_int!(u128);
impl_voltage_from_int!(usize);
impl_voltage_from_int!(i8);
impl_voltage_from_int!(i16);
impl_voltage_from_int!(i32);
impl_voltage_from_int!(i64);
impl_voltage_from_int!(i128);
impl_voltage_from_int!(isize);

impl From<Voltage> for f64 {
    fn from(v: Voltage) -> Self {
        match v {
            Voltage::Digital(vv) => vv.into(),
            Voltage::Analog(vv) => vv,
        }
    }
}

impl From<f64> for Voltage {
    fn from(f: f64) -> Self { Self::Analog(f) }
}

/*****************************************************************************/

/// Represents a HIGH/LOW state of a particular digital channel.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum DigitalState {
    LOW = 0,
    HIGH = 1,
}

impl From<DigitalState> for bool {
    fn from(s: DigitalState) -> Self {
        match s {
            DigitalState::HIGH => true,
            DigitalState::LOW => false,
        }
    }
}

impl From<bool> for DigitalState {
    fn from(b: bool) -> Self { if b { Self::HIGH } else { Self::LOW } }
}

macro_rules! impl_int_from_digitalstate {
    ( $u:ty ) => {
        impl From<DigitalState> for $u {
            fn from(s: DigitalState) -> $u {
                match s {
                    DigitalState::HIGH => 1,
                    DigitalState::LOW => 0,
                }
            }
        }
    }
}
impl_int_from_digitalstate!(u8);
impl_int_from_digitalstate!(u16);
impl_int_from_digitalstate!(u32);
impl_int_from_digitalstate!(u64);
impl_int_from_digitalstate!(u128);
impl_int_from_digitalstate!(usize);
impl_int_from_digitalstate!(i8);
impl_int_from_digitalstate!(i16);
impl_int_from_digitalstate!(i32);
impl_int_from_digitalstate!(i64);
impl_int_from_digitalstate!(i128);
impl_int_from_digitalstate!(isize);

macro_rules! impl_digitalstate_from_int {
    ( $u:ty ) => {
        impl From<$u> for DigitalState {
            fn from(b: $u) -> Self { if b > 0 { Self::HIGH } else { Self::LOW } }
        }
    }
}
impl_digitalstate_from_int!(u8);
impl_digitalstate_from_int!(u16);
impl_digitalstate_from_int!(u32);
impl_digitalstate_from_int!(u64);
impl_digitalstate_from_int!(u128);
impl_digitalstate_from_int!(usize);
impl_digitalstate_from_int!(i8);
impl_digitalstate_from_int!(i16);
impl_digitalstate_from_int!(i32);
impl_digitalstate_from_int!(i64);
impl_digitalstate_from_int!(i128);
impl_digitalstate_from_int!(isize);

impl From<DigitalState> for f64 {
    fn from(s: DigitalState) -> Self {
        match s {
            DigitalState::HIGH => 1.0,
            DigitalState::LOW => 0.0,
        }
    }
}

impl From<f64> for DigitalState {
    fn from(b: f64) -> Self { if b > 0.0 { Self::HIGH } else { Self::LOW } }
}

impl From<DigitalState> for Voltage {
    fn from(s: DigitalState) -> Self { Self::Digital(s) }
}

impl From<Voltage> for DigitalState {
    fn from(v: Voltage) -> Self {
        match v {
            Voltage::Digital(vv) => vv,
            Voltage::Analog(vv) => vv.into(),
        }
    }
}

/*****************************************************************************/

/// Handles the conversion of a built-in numerical value to a [`Voltage`],
/// depending on a particular [`Connection`].
///
/// [`Voltage`]s can then be converted as appropriate for a given `Connection`.
pub trait ToVoltage: Copy + Clone {
    fn for_conn_type(self, conn_type: ConnectionType) -> Voltage;

    fn for_conn(self, conn: &Connection) -> Voltage {
        self.for_conn_type(conn.conn_type())
    }
}

impl ToVoltage for bool {
    fn for_conn_type(self, conn_type: ConnectionType) -> Voltage {
        match conn_type {
            ConnectionType::Digital => {
                if self {
                    Voltage::Digital(DigitalState::HIGH)
                } else {
                    Voltage::Digital(DigitalState::LOW)
                }
            },
            ConnectionType::Analog => {
                if self { Voltage::Analog(1.0) } else { Voltage::Analog(0.0) }
            },
        }
    }
}

macro_rules! impl_tovoltage_for_int {
    ( $u:ty ) => {
        impl ToVoltage for $u {
            fn for_conn_type(self, conn_type: ConnectionType) -> Voltage {
                match conn_type {
                    ConnectionType::Digital => {
                        if self > 0 {
                            Voltage::Digital(DigitalState::HIGH)
                        } else {
                            Voltage::Digital(DigitalState::LOW)
                        }
                    },
                    ConnectionType::Analog => Voltage::Analog(self as f64),
                }
            }
        }
    }
}
impl_tovoltage_for_int!(u8);
impl_tovoltage_for_int!(u16);
impl_tovoltage_for_int!(u32);
impl_tovoltage_for_int!(u64);
impl_tovoltage_for_int!(u128);
impl_tovoltage_for_int!(usize);
impl_tovoltage_for_int!(i8);
impl_tovoltage_for_int!(i16);
impl_tovoltage_for_int!(i32);
impl_tovoltage_for_int!(i64);
impl_tovoltage_for_int!(i128);
impl_tovoltage_for_int!(isize);

impl ToVoltage for f64 {
    fn for_conn_type(self, conn_type: ConnectionType) -> Voltage {
        match conn_type {
            ConnectionType::Digital => {
                if self > 0.0 {
                    Voltage::Digital(DigitalState::HIGH)
                } else {
                    Voltage::Digital(DigitalState::LOW)
                }
            },
            ConnectionType::Analog => Voltage::Analog(self),
        }
    }
}

impl ToVoltage for Voltage {
    fn for_conn_type(self, conn_type: ConnectionType) -> Voltage {
        match conn_type {
            ConnectionType::Digital => {
                match self {
                    Self::Digital(s) => Self::Digital(s),
                    Self::Analog(f) => Self::Digital(f.into()),
                }
            },
            ConnectionType::Analog => {
                match self {
                    Self::Digital(s) => Self::Analog(s.into()),
                    Self::Analog(f) => Self::Analog(f),
                }
            },
        }
    }
}

impl ToVoltage for DigitalState {
    fn for_conn_type(self, conn_type: ConnectionType) -> Voltage {
        match conn_type {
            ConnectionType::Digital => Voltage::Digital(self),
            ConnectionType::Analog => Voltage::Analog(self.into()),
        }
    }
}
