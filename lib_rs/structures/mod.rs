//! Provides abstractions to handle the construction of sequences that can be
//! sent to Entangleware Control.
//!
//! Every sequence element is constrained to happen on a certain number of
//! [`Connection`]s, each of which describe a single output port from either
//! the digital or analog boards.
//! 
//! Sequence elements are modeled either as [`Event`]s, which are high-level
//! descriptions of what can happen across a number of `Connection`s, or
//! [`State`]s, which are low-level, direct descriptions of just one
//! `Connection` each.
//!
//! Sequences themselves are described by [`Sequence<T>`][Sequence] objects.
//! `Sequence<Event>`s are used by [`SuperSequence`]s for visualization and
//! record-keeping, while `Sequence<State>`s are fed to a [`Computer`] to be
//! realized physically.

use thiserror::Error;
use crate::{
    entangleware_control_link::CLError,
    entangleware_math::MathError,
};

#[derive(Debug, Error)]
pub enum EWError {
    #[error("encountered an analog connection passed to a digital structure")]
    InvalidConnectionDigital,

    #[error("encountered a digital connection passed to an analog structure")]
    InvalidConnectionAnalog,

    #[error("event ending time must be later than its beginning time")]
    InvalidEventTimes,

    #[error("data time series must be non-empty")]
    EmptyTimeSeries,

    #[error("connection label not found: {0}")]
    MissingConnectionLabel(String),

    #[error("error accessing filesystem: {0}")]
    FilesystemError(String),

    #[error("error serializing data: {0}")]
    SerializationError(String),

    #[error("error deserializing data: {0}")]
    DeserializationError(String),

    #[error("control link error {0}")]
    CLError(#[from] CLError),

    #[error("computer error {0}")]
    ComputerError(#[from] ComputerError),

    #[error("math error: {0}")]
    MathError(#[from] MathError),
}
pub type EWResult<T> = Result<T, EWError>;

/// Convert an integer to a `Vec` of 1's and 0's starting with the
/// least-significant digit.
fn to_bits(n: u32, B: usize) -> Vec<u8> {
    (0..B).map(|k| ((n >> k.min(31)) % 2) as u8).collect()
}

/// Convert an integer to a `String` of 1's and 0's starting with the
/// least-significant digit.
fn to_bitstring(n: u32, B: usize) -> String {
    to_bits(n, B).iter()
        .flat_map(|b| if *b != 0 { "1".chars() } else { "0".chars() })
        .collect()
}

/// Indent a multi-line string of text by `n * 4` spaces.
fn indent_block(s: &str, n: usize) -> String {
    let mut outstr = String::new();
    s.split('\n').for_each(|line| {
        outstr.push_str(("    ".repeat(n) + line + "\n").as_str())
    });
    outstr
}

pub mod voltage;
pub use voltage::{
    Voltage,
    ToVoltage,
    DigitalState,
};

pub mod connection;
pub use connection::{
    Connection,
    ConnectionType,
    ConnectionLayout,
};

pub mod event;
pub use event::Event;

pub mod state;
pub use state::State;

pub mod sequence;
pub use sequence::{
    ColorCycle,
    ColorCycler,
    SeqColor,
    Sequence,
};

pub mod supersequence;
pub use supersequence::SuperSequence;

pub mod computer;
pub use computer::{
    Computer,
    channel_int,
};
use computer::ComputerError;

