pub trait ErrMsg {
    fn msg(&self) -> &'static str;
}

#[macro_export]
macro_rules! mkerr {
    (
        $err:ident : { $(
            $var:ident => $msg:literal
        ),* $(,)? } => $res:ident
    ) => {
        #[derive(Clone, Copy, Debug)]
        pub enum $err {
            $( $var, )*
        }

        impl $crate::error::ErrMsg for $err {
            fn msg(&self) -> &'static str {
                match *self {
                    $( $err::$var => $msg, )*
                }
            }
        }

        impl std::fmt::Display for $err {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                f.write_str($crate::error::ErrMsg::msg(self))
            }
        }

        impl std::error::Error for $err { }

        pub type $res<T> = Result<T, $err>;
    }
}

