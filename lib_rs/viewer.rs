//! Provides [`SequenceViewer`], implementing visualization for
//! [`SuperSequence`]s.

use eframe::egui;
use egui_plot as eplot;
use indexmap::IndexMap;
use thiserror::Error;
use crate::structures::{
    connection::{
        Connection,
        ConnectionLayout,
        NamedConnection,
    },
    sequence::{
        SeqColor,
        ColorCycle,
        ColorCycler,
    },
    supersequence::SuperSequence,
    voltage::Voltage,
};

#[derive(Debug, Error)]
pub enum ViewerError {
    #[error("gui error: {0}")]
    GuiError(String),
}
pub type ViewerResult<T> = Result<T, ViewerError>;

pub type Points = Vec<(f64, f64)>;
pub type DelayPoints = (Points, Points);
pub type Series = (String, SeqColor, DelayPoints);

/// Visualizer implementation for [`SuperSequence`]s over a given
/// [`ConnectionLayout`].
///
/// The visualizer should be launched by calling [`Self::run`].
pub struct SequenceViewer {
    data: IndexMap<NamedConnection, Vec<Series>>,
    seq_labels: Vec<(String, f64)>,
    highlight_conn: Option<Connection>,
    highlight_seq: Option<String>,
}

impl SequenceViewer {
    /// Launch the visualizer to look at a [`SuperSequence`] over a given
    /// [`ConnectionLayout`].
    pub fn run(
        super_sequence: &SuperSequence,
        connections: &ConnectionLayout,
        color_cycle: Option<ColorCycle>,
    ) -> ViewerResult<()>
    {
        let native_options = eframe::NativeOptions {
            viewport: egui::viewport::ViewportBuilder {
                maximized: Some(false),
                decorations: Some(true),
                fullscreen: Some(false),
                drag_and_drop: Some(false),
                icon: None,
                position: None,
                inner_size: Some(egui::Vec2 { x: 1200.0, y: 750.0 }),
                min_inner_size: None,
                max_inner_size: None,
                resizable: Some(true),
                transparent: Some(false),
                mouse_passthrough: Some(false),
                active: Some(true),
                close_button: Some(true),
                minimize_button: Some(true),
                maximize_button: Some(true),
                window_level: None,
                ..egui::viewport::ViewportBuilder::default()
            },
            vsync: true,
            ..eframe::NativeOptions::default()
        };
        let super_sequence = super_sequence.clone();
        let connections = connections.clone();
        eframe::run_native(
            "Sequence viewer",
            native_options,
            Box::new(move |cc| {
                Ok(
                    Box::new(
                        SequenceViewer::new(
                            cc,
                            super_sequence,
                            connections,
                            color_cycle,
                        )
                    )
                )
            }),
        )
        .map_err(|e| ViewerError::GuiError(e.to_string()))
    }

    fn new(
        cc: &eframe::CreationContext<'_>,
        super_sequence: SuperSequence,
        connections: ConnectionLayout,
        color_cycle: Option<ColorCycle>,
    ) -> Self
    {
        cc.egui_ctx.set_visuals(egui::Visuals::dark());
        let mut seq_labels: Vec<(String, f64)>
            = super_sequence.iter()
            .filter_map(|(name, seq)| seq.min_time().map(|t| (name.clone(), t)))
            .collect();
        seq_labels.sort_by(|(_, time_l), (_, time_r)| time_l.total_cmp(time_r));
        let mut color_cycler
            = ColorCycler::new(color_cycle.unwrap_or(ColorCycle::Default));
        let t_end: f64 = super_sequence.max_time().unwrap_or(0.0);
        let data: IndexMap<NamedConnection, Vec<Series>>
            = super_sequence
            .singles_by_connection_viewer(&connections, Some(&mut color_cycler))
            .into_iter()
            .map(|((conn_name, conn), named_seqs)| {
                let mut cur: f64 = conn.get_default_voltage().into();
                let mut named_series: Vec<Series>
                    = named_seqs.into_iter()
                    .map(|(name, singles)| {
                        let color: SeqColor = singles.color.unwrap();
                        let (series0, series1): DelayPoints
                            = singles.into_iter()
                            .map(|single| {
                                let (t, v): ((f64, f64), Voltage)
                                    = single.get_point_single_delay().unwrap();
                                (t, v.into())
                            })
                            .flat_map(|((t0, t1), v)| {
                                let vline = [
                                    ((t0, cur), (t1, cur)),
                                    ((t0, v), (t1, v)),
                                ];
                                cur = v;
                                vline
                            })
                            .unzip();
                        (name, color, (series0, series1))
                    })
                    .collect();
                named_series.push(
                    (
                        "".to_string(),
                        SeqColor(75, 75, 75),
                        (
                            vec![(t_end, cur)],
                            vec![(t_end, cur)],
                        ),
                    )
                );
                ((conn_name, conn), named_series)
            })
            .collect();
        Self {
            data,
            seq_labels,
            highlight_conn: None,
            highlight_seq: None,
        }
    }

    fn ycoord(v: f64, conn: &Connection, k: usize) -> f64 {
        const CHANNEL_SPACE: f64 = 3.0;
        if conn.is_digital() {
            CHANNEL_SPACE * k as f64 + 1.0 + v
        } else {
            CHANNEL_SPACE * k as f64 + 1.0 + (v + 10.0) / 20.0
        }
    }

    fn channel_offset(k: usize) -> f64 {
        const CHANNEL_SPACE: f64 = 3.0;
        CHANNEL_SPACE * k as f64 + 1.0
    }

    fn connection_highlight_buttons(&mut self, ui: &mut egui::Ui) {
        egui::ScrollArea::new([true, true])
            .show(ui, |ui| {
                ui.label("Highlight connection:");
                ui.selectable_value(
                    &mut self.highlight_conn,
                    None,
                    "None",
                );
                for (conn_name, conn) in self.data.keys() {
                    ui.selectable_value(
                        &mut self.highlight_conn,
                        Some(*conn),
                        conn_name,
                    );
                }
            });
    }

    fn sequence_highlight_buttons(&mut self, ui: &mut egui::Ui) {
        egui::ScrollArea::new([true, true])
            .show(ui, |ui| {
                ui.label("Highlight sequence:");
                ui.selectable_value(
                    &mut self.highlight_seq,
                    None,
                    "None",
                );
                for (seq_name, _) in self.seq_labels.iter() {
                    ui.selectable_value(
                        &mut self.highlight_seq,
                        Some(seq_name.to_string()),
                        seq_name,
                    );
                }
            });
    }

    fn plot_channel_guides(
        k: usize,
        conn: &Connection,
        plt: &mut eplot::PlotUi,
    ) {
        let channel_offset: f64 = Self::channel_offset(k);
        let color = egui::Color32::from_rgb(127, 127, 127);
        let width = 1.0;
        plt.hline(
            eplot::HLine::new(channel_offset)
                .color(color)
                .width(width)
        );
        plt.hline(
            eplot::HLine::new(channel_offset + 1.0)
                .color(color)
                .width(width)
        );
        if conn.is_analog() {
            plt.hline(
                eplot::HLine::new(channel_offset + 0.5)
                    .color(color)
                    .style(eplot::LineStyle::Dotted { spacing: 10.0 })
                    .width(width)
            );
        }
    }

    fn plot_channel_trace(
        &self,
        bounds: eplot::PlotBounds,
        k: usize,
        named_conn: &NamedConnection,
        seqs: &[Series],
        plt: &mut eplot::PlotUi,
    ) {
        let (conn_name, conn) = named_conn;
        let mut cur_time: f64 = 0.0;
        let mut cur_val: f64
            = Self::ycoord(conn.get_default_voltage().into(), conn, k);
        let mut highlight: bool;
        let mut points_pre: Vec<[f64; 2]>;
        let mut points_post: Vec<[f64; 2]>;
        for (name, color, delay_points) in seqs.iter() {
            highlight
                = self.highlight_conn
                    .as_ref().is_some_and(|c| c == conn)
                || self.highlight_seq
                    .as_ref().is_some_and(|s| s == name);

            points_pre = vec![[cur_time, cur_val]];
            points_pre.append(
                &mut delay_points.0.iter()
                    .map(|(t, v)| [*t, Self::ycoord(*v, conn, k)]).collect());

            points_post = vec![[cur_time, cur_val]];
            points_post.append(
                &mut delay_points.1.iter()
                    .map(|(t, v)| [*t, Self::ycoord(*v, conn, k)]).collect());

            cur_time = points_post.last().unwrap()[0];
            cur_val = points_post.last().unwrap()[1];
            points_pre.push([cur_time, cur_val]);

            plt.line(
                eplot::Line::new(points_pre)
                    .color(color)
                    .style(eplot::LineStyle::Dashed { length: 10.0 })
                    .highlight(highlight)
                    .width(2.5)
            );

            plt.line(
                eplot::Line::new(points_post)
                    .color(color)
                    .style(eplot::LineStyle::Solid)
                    .highlight(highlight)
                    .width(2.5)
            );
        }

        highlight = self.highlight_conn.as_ref().is_some_and(|c| c == conn);
        plt.text(
            eplot::Text::new(
                eplot::PlotPoint {
                    x: bounds.min()[0].max(0.0),
                    y: Self::channel_offset(k) + 0.5,
                },
                conn_name,
            )
            .anchor(egui::Align2::LEFT_CENTER)
            .color(egui::Color32::from_rgb(255, 255, 255))
            .highlight(highlight)
        );
    }

    fn plot_sequence_boundaries(
        &self,
        bounds: eplot::PlotBounds,
        plt: &mut eplot::PlotUi,
    ) {
        let mut highlight: bool;
        for (seq_label, seq_time) in self.seq_labels.iter() {
            highlight
                = self.highlight_seq.as_ref()
                .is_some_and(|s| s == seq_label);
            plt.vline(
                eplot::VLine::new(*seq_time)
                    .color(egui::Color32::from_rgb(255, 255, 255))
                    .style(eplot::LineStyle::Dashed { length: 5.0 })
                    .highlight(highlight)
                    .width(1.0)
            );
            plt.text(
                eplot::Text::new(
                    eplot::PlotPoint {
                        x: *seq_time,
                        y: bounds.min()[1].max(0.0),
                    },
                    seq_label,
                )
                .anchor(egui::Align2::LEFT_BOTTOM)
                .color(egui::Color32::from_rgb(255, 255, 255))
                .highlight(highlight)
            );
        }
    }

    fn plot_connectors(
        &self,
        bounds: eplot::PlotBounds,
        plt: &mut eplot::PlotUi,
    ) {
        let N: usize = self.data.len();
        for (k, (named_conn, seqs)) in self.data.iter().enumerate() {
            Self::plot_channel_guides(N - k, &named_conn.1, plt);
            Self::plot_channel_trace(
                self, bounds, N - k, named_conn, seqs, plt);
        }
        Self::plot_sequence_boundaries(self, bounds, plt);
    }
}

impl eframe::App for SequenceViewer {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let rect = ctx.screen_rect();
        let height: f32 = rect.max.y - rect.min.y;
        let width: f32 = rect.max.x - rect.min.x;

        egui::SidePanel::right("hi")
            .exact_width(200.0_f32.min(width / 5.0))
            .resizable(false)
            .show(ctx, |ui| {
                egui::TopBottomPanel::top("hi_conn")
                    .exact_height(height / 2.0)
                    .resizable(false)
                    .show_separator_line(false)
                    .show_inside(ui, |ui| {
                        self.connection_highlight_buttons(ui);
                    });

                egui::TopBottomPanel::bottom("hi_seq")
                    .exact_height(height / 2.0)
                    .resizable(false)
                    .show_inside(ui, |ui| {
                        self.sequence_highlight_buttons(ui);
                    });
            });

        egui::CentralPanel::default()
            .show(ctx, |ui| {
                egui_plot::Plot::new("plot")
                    .height(height - 40.0)
                    .show_y(false)
                    .show(ui, |plt| {
                        let bounds: eplot::PlotBounds = plt.plot_bounds();
                        self.plot_connectors(bounds, plt);
                    });

                ui.horizontal(|ui| {
                    ui.label("Time (s)");
                });

                ui.input(|input| {
                    let ctrl_c: bool
                        = input.modifiers.command_only()
                        && input.key_down(egui::Key::C);
                    let q: bool
                        = input.modifiers.is_none()
                        && input.key_down(egui::Key::Q);

                    if ctrl_c || q {
                        let ctx = ctx.clone();
                        std::thread::spawn(move || {
                             ctx.send_viewport_cmd(
                                 egui::ViewportCommand::Close);
                        });
                    }
                });
            });
    }
}

impl From<SeqColor> for egui::Color32 {
    fn from(color: SeqColor) -> Self {
        Self::from_rgb(color.0, color.1, color.2)
    }
}

impl From<&SeqColor> for egui::Color32 {
    fn from(color: &SeqColor) -> Self {
        Self::from_rgb(color.0, color.1, color.2)
    }
}

