//! Main interface to the LabVIEW backend.
//!
//! Provides types to handle connection to the LabVIEW server interface over
//! the local network. The principal type for for this purpose is
//! [`ControlLink`], which can be instantiated and initialized directly
//! through the [`ControlLink::connect`] function.

/// Accounts for bias in voltages caused by passing through the analog isolation
/// boards.
fn unshift_analog(Vout: f64) -> f64 {
    -1.0060901 / (2.0 * (-0.0033839))
    + (if Vout > -1.0060901 / (2.0 * (-0.0033839)) { 1.0 } else { -1.0 })
    * f64::sqrt(
        f64::powi(1.0060901 / (2.0 * (-0.0033839)), 2)
        - (0.32121935 - Vout) / (-0.0033839)
    )
}

use std::{
    fs,
    io::{
        Read,
        Write,
    },
    net::SocketAddr,
    path::PathBuf,
    time::Duration,
};
use byteorder::{
    ByteOrder,
    BigEndian,
};
use socket2::{
    Domain,
    Protocol,
    SockAddr,
    Socket,
    Type,
};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum CLError {
    #[error("failed to create socket: {0}")]
    CreateSocketFail(String),

    #[error("couldn't set UDP multicaster TTL: {0}")]
    BadMulticastTTL(String),

    #[error("bad send from UDP multicaster: {0}")]
    BadSendUDPMulticaster(String),

    #[error("couldn't set read timeout: {0}")]
    BadSetReadTimeout(String),

    #[error("couldn't set write timeout: {0}")]
    BadSetWriteTimeout(String),

    #[error("couldn't bind listener socket to address: {0}")]
    BadListenerBind(String),

    #[error("failed to ready for connections: {0}")]
    BadListen(String),

    #[error("couldn't fetch listener local address: {0}")]
    BadGetLocalAddr(String),

    #[error("failed to accept incoming connection: {0}")]
    BadAcceptConnection(String),

    #[error("failed to send TCP message: {0}")]
    BadSend(String),

    #[error("failed to read TCP message: {0}")]
    BadRead(String),

    #[error("failed to find absolute path: {0}")]
    BadCanonicalize(String),

    #[error("failed to open file: {0}")]
    BadFileOpen(String),

    #[error("failed to read sequence from file: {0}")]
    BadFileRead(String),

    #[error("failed to write sequence to file: {0}")]
    BadFileWrite(String),

    #[error("length of 'element' must be a multiple of 'lengthpayload'")]
    ElementNotPayloadLength,

    #[error("unexpected return value from LV: {0:?}")]
    UnexpectedLVReturn((Vec<u8>, u64, u32)),

    #[error("board out of range: {0}")]
    BoardOutOfRange(u8),

    #[error("connector out of range: {0}")]
    ConnectorOutOfRange(u8),

    #[error("channel out of range: {0}")]
    ChannelOutOfRange(u8),

    #[error("voltage out of range: {0}")]
    VoltageOutOfRange(f64),
}
pub type CLResult<T> = Result<T, CLError>;

/// Number of digital boards.
pub const BOARDS_D: u8 = 1;

/// Number of connectors on each digital board.
pub const CONNECTORS_D: u8 = 4;

/// Number of channels on each connector on each digital board.
pub const CHANNELS_D: u8 = 32;

/// Number of analog boards.
pub const BOARDS_A: u8 = 1;

/// Number of connectors on each analog board.
pub const CONNECTORS_A: u8 = 1;

/// Number of channels on each connector on eah analog board.
pub const CHANNELS_A: u8 = 32;

/// Lower end of the analog voltage range in volts.
pub const VMIN: f64 = -10.0;

/// Upper end of the analog voltage range in volts.
pub const VMAX: f64 =  10.0;

/// Establishes a "first contact" UDP connection to the Entangleware Control
/// software.
///
/// This is used to 'multicast' to the network to find the Entangleware Control
/// software. The Entanglware Control Software, when running, will listen for
/// this multicast event. The time-to-live (MCAST_TTL) setting specified how far
/// within the intranet the multicast message will be allowed to travel.
///
/// - `1` -> only the local subnet
/// - `255` -> the entire intranet (and possibly leak to the internet)
pub struct UdpMulticaster {
    mcast_addr: SocketAddr,
    sock: Socket,
}

impl UdpMulticaster {
    pub fn bind<A>(mcast_addr: A, mcast_ttl: u32) -> CLResult<Self>
    where A: Into<SocketAddr>
    {
        let sock = Socket::new(Domain::IPV4, Type::DGRAM, Some(Protocol::UDP))
            .map_err(|e| CLError::CreateSocketFail(e.to_string()))?;
        sock.set_multicast_ttl_v4(mcast_ttl)
            .map_err(|e| CLError::BadMulticastTTL(e.to_string()))?;
        Ok(Self { mcast_addr: mcast_addr.into(), sock })
    }

    pub fn close(self) { }

    pub fn sendmsg(&self, msg: u16) -> CLResult<()> {
        let mut b_msg: [u8; 2] = [0; 2];
        BigEndian::write_u16(&mut b_msg, msg);
        self.sock.send_to(&b_msg, &self.mcast_addr.into())
            .map_err(|e| CLError::BadSendUDPMulticaster(e.to_string()))?;
        Ok(())
    }
}

/// Main communication channel to the Entangleware Control software.
///
/// This is used to create a TCP server to which the Entangleware Control
/// software will be a client. The Entangleware software will receive the
/// location of this server after first contact has been established via a UDP
/// multicast message. After the Entangleware Control software attempts to
/// connect, the connection is used to create the actual TCP endpoint used for
/// communication to/from the Entangleware Control software.
pub struct TcpServer {
    listensock: Socket,
    serverport: u16,
}

impl TcpServer {
    pub fn bind<A>(init_addr: A, timeout: Option<f64>, backlog: Option<i32>)
        -> CLResult<Self>
    where A: Into<SocketAddr>
    {
        let listensock = Socket::new(Domain::IPV4, Type::STREAM, None)
            .map_err(|e| CLError::CreateSocketFail(e.to_string()))?;
        listensock.set_read_timeout(timeout.map(Duration::from_secs_f64))
            .map_err(|e| CLError::BadSetReadTimeout(e.to_string()))?;
        listensock.set_write_timeout(timeout.map(Duration::from_secs_f64))
            .map_err(|e| CLError::BadSetWriteTimeout(e.to_string()))?;
        listensock.bind(&init_addr.into().into())
            .map_err(|e| CLError::BadListenerBind(e.to_string()))?;
        listensock.listen(backlog.unwrap_or(1))
            .map_err(|e| CLError::BadListen(e.to_string()))?;
        let serverport: u16
            = listensock.local_addr()
                .map_err(|e| CLError::BadGetLocalAddr(e.to_string()))?
            .as_socket()
            .expect("TCP server port is unix for some reason")
            .port();
        Ok(Self { listensock, serverport })
    }

    pub fn close(self) { }

    pub fn accept(&self) -> CLResult<(TcpEndPoint, SocketAddr)> {
        let (sock, addr): (Socket, SockAddr)
            = self.listensock.accept()
            .map_err(|e| CLError::BadAcceptConnection(e.to_string()))?;
        Ok(
            (
                TcpEndPoint::new(sock)?,
                addr.as_socket()
                    .expect("TCP stream address is unix for some reason"),
            )
        )
    }
}

/// TCP endpoint for communication to/from the Entangleware Control software.
///
/// This is the endpoint to which the TCP server passes the connection. The
/// endpoint will handle most communication to/from the Entangleware Control
/// software.
pub struct TcpEndPoint {
    sock: Socket,
}

impl TcpEndPoint {
    fn new(sock: Socket) -> CLResult<Self> {
        sock.set_read_timeout(Some(Duration::from_secs_f64(30.0)))
            .map_err(|e| CLError::BadSetReadTimeout(e.to_string()))?;
        sock.set_write_timeout(Some(Duration::from_secs_f64(30.0)))
            .map_err(|e| CLError::BadSetWriteTimeout(e.to_string()))?;
        Ok(Self { sock })
    }

    pub fn close(self) { }

    pub fn set_timeout(&self, timeout: f64) -> CLResult<()> {
        self.sock.set_read_timeout(Some(Duration::from_secs_f64(timeout)))
            .map_err(|e| CLError::BadSetReadTimeout(e.to_string()))?;
        self.sock.set_write_timeout(Some(Duration::from_secs_f64(timeout)))
            .map_err(|e| CLError::BadSetReadTimeout(e.to_string()))?;
        Ok(())
    }

    pub fn sendmsg(&self, msg: &[u8], msgid: u64, msgtype: u32)
        -> CLResult<()>
    {
        const CHUNK_SIZE: usize = 512;

        let mut b_msgid: [u8; 8] = [0; 8];
        BigEndian::write_u64(&mut b_msgid, msgid);
        let mut b_msgtype: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut b_msgtype, msgtype);
        let mut b_msglen: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut b_msglen, msg.len() as u32);
        let complete_msg: Vec<u8> = [
            Vec::<u8>::from(b_msgid),
            Vec::<u8>::from(b_msgtype),
            Vec::<u8>::from(b_msglen),
            Vec::<u8>::from(msg),
        ].concat();
        let mut bytes_to_send: usize = complete_msg.len();
        let mut start_addr: usize = 0;
        let mut last_elem: usize;
        let mut bytes_sent: usize;
        while bytes_to_send > 0 {
            last_elem = (start_addr + CHUNK_SIZE).min(complete_msg.len());
            bytes_sent = self.sock.send(&complete_msg[start_addr..last_elem])
                .map_err(|e| CLError::BadSend(e.to_string()))?;
            start_addr += bytes_sent;
            bytes_to_send -= bytes_sent;
        }
        Ok(())
    }

    pub fn getmsg(&mut self) -> CLResult<(Vec<u8>, u64, u32)> {
        let mut header: [u8; 16] = [0; 16];
        self.sock.read(&mut header)
            .map_err(|e| CLError::BadRead(e.to_string()))?;
        let msgid: u64 = BigEndian::read_u64(&header[0..8]);
        let msgtype: u32 = BigEndian::read_u32(&header[8..12]);
        let mut msglen: usize = BigEndian::read_u32(&header[12..16]) as usize;
        let mut msg: Vec<std::mem::MaybeUninit<u8>>
            = (0..msglen).map(|_| std::mem::MaybeUninit::new(0)).collect();
        let mut nbytes: usize;
        while msglen > 0 {
            nbytes = self.sock.recv(&mut msg)
                .map_err(|e| CLError::BadRead(e.to_string()))?;
            msglen -= nbytes;
        }
        let msg: Vec<u8> = unsafe {
            msg.into_iter().map(|b| b.assume_init()).collect()
        };
        Ok((msg, msgid, msgtype))
    }
}

/// High-level interface to handle connections and messages to the backend.
pub struct ControlLink {
    tcp_server: TcpServer,
    udp_local: UdpMulticaster,
    tcp_endpoint: TcpEndPoint,
    sequence: EWSequence,
}

impl ControlLink {
    pub fn connect<A>(timeout_sec: Option<f64>, addr: A, ttl: Option<u32>)
        -> CLResult<Self>
    where A: Into<SocketAddr>
    {
        let udp_local = UdpMulticaster::bind(addr, ttl.unwrap_or(1))?;
        let tcp_server
            = TcpServer::bind(([0, 0, 0, 0], 0), Some(1.0), Some(1))?;
        udp_local.sendmsg(tcp_server.serverport)?;
        let (tcp_endpoint, _tcp_addr): (TcpEndPoint, SocketAddr)
            = tcp_server.accept()?;
        if let Some(timeout) = timeout_sec {
            tcp_endpoint.set_timeout(timeout)?;
        }
        let sequence = EWSequence::default();
        Ok(Self { tcp_server, udp_local, tcp_endpoint, sequence })
    }

    pub fn disconnect(self) { }

    pub fn set_debug_mode(&mut self, onoff: bool) -> CLResult<()> {
        const LV_INSTRUCT: u32 = 0x15;
        let active: &[u8] = if onoff { b"True" } else { b"False" };
        self.tcp_endpoint.sendmsg(active, 0, LV_INSTRUCT)?;
        Ok(())
    }

    pub fn is_building(&self) -> bool { self.sequence.building }

    pub fn build_sequence(&mut self) { self.sequence.building = true; }

    pub fn clear_sequence(&mut self) { self.sequence.clear(); }

    pub fn run_sequence(&mut self, printflag: bool) -> CLResult<()> {
        const LV_INSTRUCT: u32 = 0x16;
        const NUMBER_CYCLES: i32 = 1; // don't change this (feature not yet implemented)

        let mut tosend: [u8; 4] = [0; 4];
        BigEndian::write_i32(&mut tosend, NUMBER_CYCLES);
        let mut tcpmessage: Vec<u8> = tosend.to_vec();
        let message_length: usize
            = self.sequence.seqendindex * self.sequence.lengthpayload;
        tcpmessage.append(&mut self.sequence.seq[..message_length].to_vec());
        self.tcp_endpoint.sendmsg(&tcpmessage, 0, LV_INSTRUCT)?;
        if printflag {
            println!(
                "[entangleware] sent {} events to Entangleware Control",
                self.sequence.seqendindex,
            );
        }

        let (runreturn, _, _): (Vec<u8>, _, _) = self.tcp_endpoint.getmsg()?;
        let runtime: f64 = BigEndian::read_f64(&runreturn);
        if printflag {
            println!("[entangleware] expected runtime: {}", runtime);
        }
        let path_to_temp: PathBuf
            = fs::canonicalize(".")
            .map_err(|e| CLError::BadCanonicalize(e.to_string()))?
            .join("LastCompiledRun.dat");
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .append(false)
            .open(path_to_temp)
            .map_err(|e| CLError::BadFileOpen(e.to_string()))?
            .write_all(&tcpmessage)
            .map_err(|e| CLError::BadFileWrite(e.to_string()))?;

        self.tcp_endpoint.set_timeout(runtime + 20.0)?;
        self.tcp_endpoint.getmsg()?;
        self.tcp_endpoint.set_timeout(10.0)?;

        Ok(())
    }

    pub fn rerun_last_sequence(&mut self, printflag: bool) -> CLResult<()> {
        const LV_INSTRUCT: u32 = 0x16;

        let path_to_temp: PathBuf
            = fs::canonicalize(".")
            .map_err(|e| CLError::BadCanonicalize(e.to_string()))?
            .join("LastCompiledRun.dat");
        let mut tcpmessage: Vec<u8> = Vec::new();
        fs::OpenOptions::new()
            .read(true)
            .open(path_to_temp)
            .map_err(|e| CLError::BadFileOpen(e.to_string()))?
            .read_to_end(&mut tcpmessage)
            .map_err(|e| CLError::BadFileRead(e.to_string()))?;

        self.tcp_endpoint.sendmsg(&tcpmessage, 0, LV_INSTRUCT)?;
        let (runreturn, _, _): (Vec<u8>, _, _) = self.tcp_endpoint.getmsg()?;
        let runtime: f64 = BigEndian::read_f64(&runreturn);
        if printflag {
            println!("[entangleware] expected runtime: {}", runtime);
        }
        self.tcp_endpoint.set_timeout(runtime + 20.5)?;
        self.tcp_endpoint.getmsg()?;
        self.tcp_endpoint.set_timeout(10.0)?;

        Ok(())
    }

    pub fn run_sequence_chain(&mut self, printflag: bool) -> CLResult<()> {
        const LV_INSTRUCT: u32 = 0x16;
        const NUMBER_CYCLES: i32 = 1; // don't change this (feature not yet implemented)

        if !self.sequence.seqchainfirstcall {
            self.tcp_endpoint.set_timeout(
                self.sequence.seqchainlastruntime + 20.5)?;
            let donemsg: (Vec<u8>, u64, u32) = self.tcp_endpoint.getmsg()?;
            if donemsg != (b"Done".to_vec(), 15, 15) {
                return Err(CLError::UnexpectedLVReturn(donemsg));
            }
            self.tcp_endpoint.set_timeout(10.0)?;
        }

        let mut tosend: [u8; 4] = [0; 4];
        BigEndian::write_i32(&mut tosend, NUMBER_CYCLES);
        let mut tcpmessage: Vec<u8> = tosend.to_vec();
        let message_length: usize
            = self.sequence.seqendindex * self.sequence.lengthpayload;
        tcpmessage.append(&mut self.sequence.seq[..message_length].to_vec());
        self.tcp_endpoint.sendmsg(&tcpmessage, 0, LV_INSTRUCT)?;
        if printflag {
            println!(
                "[entangleware] send {} events to Entangleware Control",
                self.sequence.seqendindex,
            );
        }
        self.sequence.clear();
        self.sequence.building = false;

        let (runreturn, _, _): (Vec<u8>, _, _) = self.tcp_endpoint.getmsg()?;
        let runtime: f64 = BigEndian::read_f64(&runreturn);
        self.sequence.seqchainlastruntime = runtime;
        if printflag {
            println!("[entangleware] expected runtime: {}", runtime);
        }
        let path_to_temp: PathBuf
            = fs::canonicalize(".")
            .map_err(|e| CLError::BadCanonicalize(e.to_string()))?
            .join("LastCompiledRun.dat");
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .append(false)
            .open(path_to_temp)
            .map_err(|e| CLError::BadFileOpen(e.to_string()))?
            .write_all(&tcpmessage)
            .map_err(|e| CLError::BadFileWrite(e.to_string()))?;
        self.sequence.seqchainfirstcall = false;

        Ok(())
    }

    pub fn stop_sequence(&mut self) -> CLResult<()> {
        const LV_INSTRUCT: u32 = 0x13;
        const NUMBER_CYCLES: i32 = 1;
        let mut tosend: [u8; 4] = [0; 4];
        BigEndian::write_i32(&mut tosend, NUMBER_CYCLES);
        self.tcp_endpoint.sendmsg(&tosend, 0, LV_INSTRUCT)?;
        Ok(())
    }

    pub fn set_digital_state(
        &mut self,
        seq_time: f64,
        board: u8,
        connector: u8,
        channel_mask: u32,
        output_enable_state: u32,
        output_state: u32,
    ) -> CLResult<()>
    {
        const LV_INSTRUCT: u32 = 0x14;

        (0..BOARDS_D).contains(&board)
            .then_some(()).ok_or(CLError::BoardOutOfRange(board))?;
        (0..CONNECTORS_D).contains(&connector)
            .then_some(()).ok_or(CLError::ConnectorOutOfRange(connector))?;

        let address: u32
            = (0x01_u32 << 24) | ((board as u32) << 8) | (connector as u32);
        let mut _seq_time: [u8; 8] = [0; 8];
        BigEndian::write_f64(&mut _seq_time, seq_time);
        let mut _address: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut _address, address);
        let mut _channel_mask: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut _channel_mask, channel_mask);
        let mut _output_enable_state: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut _output_enable_state, output_enable_state);
        let mut _output_state: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut _output_state, output_state);
        let tosend: Vec<u8> = [
            Vec::<u8>::from(_seq_time),
            Vec::<u8>::from(_address),
            Vec::<u8>::from(_channel_mask),
            Vec::<u8>::from(_output_enable_state),
            Vec::<u8>::from(_output_state),
        ].concat();
        if self.sequence.building {
            self.sequence.add_element(&tosend)?;
        } else {
            self.tcp_endpoint.sendmsg(&tosend, 0, LV_INSTRUCT)?;
        }
        Ok(())
    }

    pub fn set_analog_state(
        &mut self,
        seq_time: f64,
        board: u8,
        channel: u8, // in 0..32
        value: f64,
    ) -> CLResult<()>
    {
        const LV_INSTRUCT: u32 = 0x14;

        (0..BOARDS_A).contains(&board)
            .then_some(()).ok_or(CLError::BoardOutOfRange(board))?;
        (0..CHANNELS_A).contains(&channel)
            .then_some(()).ok_or(CLError::ChannelOutOfRange(channel))?;
        (VMIN..=VMAX).contains(&value)
            .then_some(()).ok_or(CLError::VoltageOutOfRange(value))?;
        let value: f64 = unshift_analog(value);

        let address: u32 = (0x02_u32 << 24) | ((board as u32) << 8);
        // v most-sig byte is the control: 1 = enabled, 0 = disabled
        let channel_mask: u32 = (0x01_u32 << 24) | (channel as u32);
        let mut _seq_time: [u8; 8] = [0; 8];
        BigEndian::write_f64(&mut _seq_time, seq_time);
        let mut _address: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut _address, address);
        let mut _channel_mask: [u8; 4] = [0; 4];
        BigEndian::write_u32(&mut _channel_mask, channel_mask);
        let mut _value: [u8; 8] = [0; 8];
        BigEndian::write_f64(&mut _value, value);
        let tosend: Vec<u8> = [
            Vec::<u8>::from(_seq_time),
            Vec::<u8>::from(_address),
            Vec::<u8>::from(_channel_mask),
            Vec::<u8>::from(_value),
        ].concat();
        if self.sequence.building {
            self.sequence.add_element(&tosend)?;
        } else {
            self.tcp_endpoint.sendmsg(&tosend, 0, LV_INSTRUCT)?;
        }
        Ok(())
    }
}

/// State-ful staging ground for sequences to be sent to the Entangleware
/// Control software.
#[derive(Clone)]
pub struct EWSequence {
    pub building: bool,
    seqendindex: usize,
    lengthpayload: usize,
    lengthsequence: usize,
    sizeofbytearray: usize,
    seq: Vec<u8>,
    seqchainfirstcall: bool,
    seqchainlastruntime: f64,
}

impl Default for EWSequence {
    fn default() -> Self { Self::new() }
}

impl EWSequence {
    pub fn new() -> Self {
        EWSequence {
            building: false,
            seqendindex: 0,
            lengthpayload: 24,
            lengthsequence: 1048576, // = 2^20
            sizeofbytearray: 25165824, // = 24 * 2^20
            seq: vec![0_u8; 25165824], // size = sizeofbytearray
            seqchainfirstcall: true,
            seqchainlastruntime: 0.0,
        }
    }

    pub fn add_element(&mut self, element: &[u8]) -> CLResult<()> {
        let length_element: usize = element.len();
        if length_element % self.lengthpayload != 0 {
            return Err(CLError::ElementNotPayloadLength);
        }

        let length_buffer: usize = self.seq.len();
        let start_index: usize = self.seqendindex * self.lengthpayload;
        let length_empty_seq: usize = length_buffer - start_index;

        if length_element > length_empty_seq {
            let addl: usize
                = self.sizeofbytearray
                * f64::ceil(
                    length_element as f64
                    / self.sizeofbytearray as f64
                ) as usize;
            self.seq.append(&mut vec![0_u8; addl]);
        }

        self.seq.iter_mut()
            .skip(start_index)
            .take(length_element)
            .zip(element.iter())
            .for_each(|(seq_byte, elem_byte)| {
                *seq_byte = elem_byte.to_be()
            });
        self.seqendindex += length_element / self.lengthpayload;

        Ok(())
    }

    pub fn clear(&mut self) {
        self.building = false;
        self.seq = vec![0_u8; 25165824];
        self.seqendindex = 0;
    }
}

