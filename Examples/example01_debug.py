import sys
import os.path as path
sys.path.insert(0, path.join(path.dirname(path.abspath(__file__)), ".."))
import lib.entangleware_control_link as ew
import time

ew.connect(1.0)  # connects to the Entangleware Control software via UDP and TCP

ew.debug_mode(True)  # Tells the Entangleware Control debug window to launch
time.sleep(2.0)
# ew.debug_mode(False)  # Tells the Entangleware Control debug window to close (user can also close it manually)

ew.disconnect()  # Disconnects from the entanglware Control software