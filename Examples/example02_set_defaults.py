import sys
import os.path as path
sys.path.insert(0, path.join(path.dirname(path.abspath(__file__)), ".."))
import lib.entangleware_control_link as ew
import time

ew.connect(1.0)  # connects to the Entangleware Control software via UDP and TCP

ew.debug_mode(True)  # Tells the Entangleware Control debug window to launch
time.sleep(1.0)

# arguments are time (unused), board, connector, channel mask, output enable, and output state
# the mask, enable and state are interpreted as U32 numbers. Each bit corresponds to a channel on the 7820R connector
# the digital lines on the 7820R have output drivers which can be enabled or disabled (high-Z)
# use the mask to select which channels should be updated
# use the 'output enable' to select which channels should drive their outputs
# use 'output state' to specify the output state:
ew.set_digital_state(float('NaN'), 0, 0, 0xFFFFFFFF, 0xFFFFFFFF, 0x0000000A)
ew.set_digital_state(float('NaN'), 0, 1, 0xFFFFFFFF, 0xFFFFFFFF, 0x0000000B)
ew.set_digital_state(float('NaN'), 0, 2, 0xFFFFFFFF, 0xFFFFFFFF, 0x0000000C)
ew.set_digital_state(float('NaN'), 0, 3, 0xFFFFFFFF, 0xFFFFFFFF, 0x0000000D)
time.sleep(1.0)
# The command below will select all of the 28 upper bits (4:31) and change their output state to 0x5555555
# Notice that its mask leaves the lower nibble (bits 0:3) alone: they will stay at 0xA and **NOT** change to 0xF
ew.set_digital_state(float('NaN'), 0, 0, 0xFFFFFFF0, 0xFFFFFFFF, 0x5555555F)
time.sleep(1.0)

# This will slowly, and ~asynchronously change all analog channels from 0 to 9 Volts in steps of 1 Volt
for value in range(10):  # change output value from 0 thru 9
    for index in range(32):  # update all 32 channels on the 6738 analog board
        ew.set_analog_state(float('NaN'), 0, index, value)
    time.sleep(0.5)  # sleep to see the Board Default States change in the Entanglware Debug window and on a DMM

# reset all channels back to 0.0 Volts
for index in range(32):  # update all 32 channels on the 6738 analog board
    ew.set_analog_state(float('NaN'), 0, index, 0)

ew.disconnect()  # Disconnects from the entanglware Control software