import sys
import os.path as path
sys.path.insert(0, path.join(path.dirname(path.abspath(__file__)), ".."))
import lib.entangleware_control_link as ew

ew.connect(1.0)  # connects to the Entangleware Control software via UDP and TCP

ew.debug_mode(True)  # Tells the Entangleware Control debug window to launch
ew.set_digital_state(0.000, 0, 0, 0x1, 0x1, 0x0)  # This ensures the default state of channel 0 is low

# This tells the Entangleware software that the digital and analog methods after this line until python reaches
# 'run_sequence'/'clear_sequence' are deterministically timed.
ew.build_sequence()  # begins queueing digital and analog 'set state' data for deterministic execution

# a 1ms wide pulse on channel 0
ew.set_digital_state(0.000, 0, 0, 0xF, 0xF, 0x1)
ew.set_digital_state(0.001, 0, 0, 0xF, 0xF, 0x0)

# This creates a digital ramp on the lowest 4 bits of board 0, connector 0
for index in range(16):
    ew.set_digital_state(0.1250 * (index + 1), 0, 0, 0xF, 0xF, index + 1)


# This will run the queued sequence: all items in queue will be sorted and duplicates removed by the
# Entangleware Control software.  The print statement is to show the reply from the Entagleware software.
print(ew.run_sequence())  # runs the queued sequence and blocks until the Entangleware software finishes the sequence
ew.clear_sequence()  # this stops queueing and clears the previously queued sequence

ew.disconnect()  # Disconnects from the entanglware Control software